//
//  Globals.swift
//  EPSS
//
//  Created by Informatica EPSS on 1/27/21.
//  Copyright © 2021 EPSS. All rights reserved.
//

import Foundation

struct Globals {
    
    static let logo = "logo"
    static let logo_white = "logo_white"
    static let chat_pattern = "pattern"
    
    static let img_medical_assistance = "telemedicina"
    static let img_make_appointment = "schedule_appointment"
    static let img_chat_with_us = "chat_with_us"
    static let img_medical_network = "red_medica"
    
    static let img_general_medicine = "medico_general"
    static let img_gynecology = "ginecologo"
    static let img_pediatrics = "pediatra"
    
    static let img_doctor = "medico"
    static let img_hospital = "hospital"
    static let img_laboratory = "laboratorio"
    static let img_diagnostic_center = "diagnostico"
    
}
