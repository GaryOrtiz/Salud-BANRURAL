//
//  ImageCardTableViewCell.swift
//  EPSS
//
//  Created by Informatica EPSS on 3/9/21.
//  Copyright © 2021 EPSS. All rights reserved.
//

import UIKit

class ImageCardTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var stackView: UIStackView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    public func setupCellWith(imageCard: ImageCard) {
        titleLabel.text = imageCard.title
        cardImageView.image = UIImage(named: imageCard.image)
    }
    
}
