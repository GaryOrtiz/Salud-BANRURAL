//
//  PDFViewerViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 1/7/21.
//  Copyright © 2021 EPSS. All rights reserved.
//

import UIKit
import PDFKit

class PDFViewerViewController: UIViewController {

    var pdfUrl: URL?
    private var document: PDFDocument!
    private var outline: PDFOutline?
    private var pdfView = PDFView()
    
    init(with pdfUrl: URL) {
        self.pdfUrl = pdfUrl
//        self.document = PDFDocument(url: pdfUrl)
//        self.outline = document.outlineRoot
//        pdfView.document = document
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        view.addSubview(pdfView)

        pdfView.translatesAutoresizingMaskIntoConstraints = false
        pdfView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        pdfView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        pdfView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        pdfView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
                
        if let pdfUrl = self.pdfUrl, let document = PDFDocument(url: pdfUrl) {
            self.document = PDFDocument(url: pdfUrl)
            self.outline = document.outlineRoot
            pdfView.document = document
            pdfView.displayDirection = .vertical
//            pdfView.usePageViewController(true)
            pdfView.pageBreakMargins = UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
            pdfView.autoScales = true
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now()+3) {
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    


}
