//
//  NewChatPopUp.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/28/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD

class NewChatPopUp: UIView {
    
    let spinner = JGProgressHUD(style: .dark)
    var queueId: Int = 0
    
    var typeOfPopup: String? {
        didSet {
            guard let type = typeOfPopup else {
                return
            }
            if type == "medicalAssistance" {
                titleLabel.text = "Asistencia médica"
                messageTextView.text = "Buenos días, quiero hablar con un médico"
                queueId = 6
            }
            else if type == "chat" {
                titleLabel.text = "Coordinar cita"
                messageTextView.text = "Buenos días, quiero coordinar una cita"
                queueId = 1
            }
        }
    }
    
    fileprivate let titleLabel : UILabel = {
        let label  = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        label.textAlignment = .center
        label.text = ""
        label.numberOfLines = 3
        label.backgroundColor = .primaryColor
        label.textColor = .white
        label.clipsToBounds = true
        label.layer.cornerRadius = 15
        label.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        return label
    }()
    
    fileprivate let messageTextView : UITextView = {
        let textView  = UITextView()
        textView.translatesAutoresizingMaskIntoConstraints = false
        textView.font = UIFont.systemFont(ofSize: 16, weight: .medium)
        textView.text = ""
        textView.backgroundColor = .white
        return textView
    }()
    
    fileprivate let sendButton : UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .acceptButtonColor
        button.setTitle("Enviar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(createChat), for: .touchUpInside)
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.layer.maskedCorners = [.layerMaxXMaxYCorner]
        return button
    }()
    
    fileprivate let closeButton : UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .cancelButtonColor
        button.setTitle("Cerrar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.addTarget(self, action: #selector(animateOut), for: .touchUpInside)
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.layer.maskedCorners = [.layerMinXMaxYCorner]
        return button
    }()
    
    fileprivate let container : UIView = {
        let v = UIView()
        v.translatesAutoresizingMaskIntoConstraints = false
        v.backgroundColor = .white
        v.layer.cornerRadius = 15
        
        return v
    }()
    
    fileprivate lazy var stackButtons: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [closeButton, sendButton])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        return stack
    }()
    
    fileprivate lazy var stack: UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titleLabel, messageTextView])
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.axis = .vertical
        stack.backgroundColor = .white
        return stack
    }()
    
    @objc fileprivate func animateOut(){
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn) {
            self.container.transform = CGAffineTransform(translationX: 0, y: -self.frame.height)
            self.alpha = 0
        } completion: { (complete) in
            if complete {
                self.removeFromSuperview()
            }
        }

    }
    
    @objc fileprivate func animateIn(){
        self.container.transform = CGAffineTransform(translationX: 0, y: -self.frame.height)
        self.alpha = 0
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn) {
            self.container.transform = .identity
            self.alpha = 1
        }

    }
    
    @objc fileprivate func createChat(){
        
        guard let newMessage = messageTextView.text,
              !newMessage.isEmpty else {
            self.presentErrorAlert(mensaje: "Por favor ingresa un mensaje")
            return
        }
        
        self.spinner.show(in: (parentContainerViewController()?.view)!)
        
        let newChatRequest = NewChatRequest(texto_inicio: messageTextView.text)
        
        SN.post(endpoint: "\(EndPoints.newChat)/\(queueId)", model: newChatRequest) { ( response: SNResultWithEntity<ChatResponse, ErrorResponse>) in
            
            self.spinner.dismiss()
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    self.animateOut()
                    let vc = ChatViewController(with: data.chat.id)
                    vc.navigationItem.largeTitleDisplayMode = .never
                    self.parentContainerViewController()?.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    self.presentErrorAlert(mensaje: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(mensaje: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(mensaje: entity.mensaje)
            }
            
        }
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.alpha = 0
        self.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(animateOut)))
        self.backgroundColor = .blackTransparentColor
        self.frame = UIScreen.main.bounds
        self.addSubview(container)
        
        container.topAnchor.constraint(equalTo: self.topAnchor, constant: 150).isActive = true
        container.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        container.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.75).isActive = true
        container.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 0.3).isActive = true
        
        container.addSubview(stack)
        container.addSubview(stackButtons)
        
//        container.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        stack.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        stack.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        stack.centerYAnchor.constraint(equalTo: container.centerYAnchor).isActive = true
//        container.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
        stack.heightAnchor.constraint(equalTo: container.heightAnchor, multiplier: 0.5).isActive = true
        
        stackButtons.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        stackButtons.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        stackButtons.heightAnchor.constraint(equalToConstant: 50).isActive = true
        stackButtons.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
        
        titleLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
        titleLabel.heightAnchor.constraint(equalToConstant: 75).isActive = true
        titleLabel.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        
        animateIn()
        
        messageTextView.becomeFirstResponder()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func presentErrorAlert(mensaje: String){
        let alert = UIAlertController(title: "Oops!", message: mensaje, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        parentContainerViewController()?.present(alert, animated: true)
    }
    
    
    
}
