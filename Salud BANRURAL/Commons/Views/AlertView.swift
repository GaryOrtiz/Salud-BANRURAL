//
//  ConfirmPopUp.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation
import Simple_Networking
import JGProgressHUD

class AlertView: UIView {
    
    var delegate: OkAlertViewDelegate?
    
    enum type {
        case success, error
    }
    
    fileprivate let iconContainer: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate let iconView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    fileprivate let imageView: UIImageView = {
        let image = UIImage(named: "checkmark")
        let imageView = UIImageView(image: image)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    fileprivate let titleLabel : UILabel = {
        let label  = UILabel()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        label.numberOfLines = 0
        label.text = ""
        return label
    }()
    
    fileprivate let subTitleLabel : UILabel = {
        let label  = UILabel()
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.numberOfLines = 0
        label.text = ""
        return label
    }()
    
    public let okButton : UIButton = {
        let button = UIButton(type: .system)
        button.backgroundColor = .primaryColor
        button.setTitle("Aceptar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.layer.maskedCorners = [.layerMinXMaxYCorner, .layerMaxXMaxYCorner]
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    fileprivate let container : UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = .white
        view.layer.cornerRadius = 15
        return view
    }()
    
    @objc fileprivate func animateOut(){
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn) {
            self.container.transform = CGAffineTransform(translationX: 0, y: -self.frame.height)
            self.alpha = 0
        } completion: { (complete) in
            if complete {
                self.removeFromSuperview()
            }
        }

    }
    
    @objc fileprivate func animateIn(){
        self.container.transform = CGAffineTransform(translationX: 0, y: -self.frame.height)
        self.alpha = 0
        UIView.animate(withDuration: 0.7, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 1, options: .curveEaseIn) {
            self.container.transform = .identity
            self.alpha = 1
        }

    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.alpha = 0
        self.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        self.frame = UIScreen.main.bounds
        self.addSubview(container)
        
        container.backgroundColor = .white
        container.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        container.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        container.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.80).isActive = true
        container.heightAnchor.constraint(equalTo: self.widthAnchor, multiplier: 0.60).isActive = true
        
        iconContainer.addSubview(iconView)
        iconView.addSubview(imageView)
        container.addSubview(iconContainer)
        container.addSubview(titleLabel)
        container.addSubview(subTitleLabel)
        container.addSubview(okButton)
        
        iconContainer.backgroundColor = .white
        iconContainer.centerXAnchor.constraint(equalTo: container.centerXAnchor).isActive = true
        iconContainer.topAnchor.constraint(equalTo: container.topAnchor, constant: -30).isActive = true
        iconContainer.heightAnchor.constraint(equalToConstant: 90).isActive = true
        iconContainer.widthAnchor.constraint(equalToConstant: 90).isActive = true
        iconContainer.clipsToBounds = true
        iconContainer.layer.cornerRadius = 45
        
        iconView.backgroundColor = .primaryColor
        iconView.centerXAnchor.constraint(equalTo: iconContainer.centerXAnchor).isActive = true
        iconView.centerYAnchor.constraint(equalTo: iconContainer.centerYAnchor).isActive = true
        iconView.heightAnchor.constraint(equalToConstant: 70).isActive = true
        iconView.widthAnchor.constraint(equalToConstant: 70).isActive = true
        iconView.clipsToBounds = true
        iconView.layer.cornerRadius = 35
        
        imageView.centerXAnchor.constraint(equalTo: iconView.centerXAnchor).isActive = true
        imageView.centerYAnchor.constraint(equalTo: iconView.centerYAnchor).isActive = true
        imageView.heightAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.widthAnchor.constraint(equalToConstant: 50).isActive = true
        imageView.tintColor = .white
        
        titleLabel.backgroundColor = .white
        titleLabel.topAnchor.constraint(equalTo: iconView.bottomAnchor, constant: 20).isActive = true
//        titleLabel.bottomAnchor.constraint(equalTo: subTitleLabel.topAnchor, constant: 20).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 20).isActive = true
        titleLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -20).isActive = true
        
        subTitleLabel.backgroundColor = .white
        subTitleLabel.topAnchor.constraint(equalTo: titleLabel.bottomAnchor, constant: 10).isActive = true
//        subTitleLabel.bottomAnchor.constraint(equalTo: okButton.topAnchor).isActive = true
        subTitleLabel.leadingAnchor.constraint(equalTo: container.leadingAnchor, constant: 20).isActive = true
        subTitleLabel.trailingAnchor.constraint(equalTo: container.trailingAnchor, constant: -20).isActive = true
        
        okButton.leadingAnchor.constraint(equalTo: container.leadingAnchor).isActive = true
        okButton.trailingAnchor.constraint(equalTo: container.trailingAnchor).isActive = true
//        okButton.topAnchor.constraint(equalTo: subTitleLabel.bottomAnchor, constant: 20).isActive = true
        okButton.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
        okButton.heightAnchor.constraint(equalToConstant: 60).isActive = true
        okButton.addTarget(self, action: #selector(okButtonTapped), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    @objc func okButtonTapped(){
        delegate?.okButtonTapped()
        animateOut()
    }
    
    func show(title:String, subTitle: String, type: type){
        if type == .success {
            imageView.image = UIImage(named: "checkmark")
        }
        else if type == .error
        {
            imageView.image = UIImage(named: "xmark")
        }
        titleLabel.text = title
        subTitleLabel.text = subTitle
        animateIn()
    }
    
    func show(title:String, subTitle: String, type: type, iconColor:UIColor, titleColor: UIColor, buttonColor: UIColor){
        if type == .success {
            imageView.image = UIImage(named: "checkmark")
        }
        else if type == .error
        {
            imageView.image = UIImage(named: "xmark")
        }
        iconView.backgroundColor = iconColor
        titleLabel.textColor = titleColor
        okButton.backgroundColor = buttonColor
        titleLabel.text = title
        subTitleLabel.text = subTitle
        animateIn()
    }
    
}

