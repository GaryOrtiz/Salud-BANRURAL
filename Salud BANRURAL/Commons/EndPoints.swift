//
//  EndPoints.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct EndPoints{
    static let domain = "https://bantrabdev.infoutilitygt.com/api/v2"
    static let login = EndPoints.domain + "/users/login"
    static let registrarse = EndPoints.domain + "/users/registro"
    static let ayudaRegistro = EndPoints.domain + "/users/ayuda-registro"
    static let reestablecerPassword = EndPoints.domain + "/users/reestablecer-password"
    static let logout = EndPoints.domain + "/users/logout"
    
    static let contrato = EndPoints.domain + "/contratos/contrato"
    
    static let chats = EndPoints.domain + "/chats/listado"
    static let newChat = EndPoints.domain + "/chats/iniciar"
    static let chat = EndPoints.domain + "/chats/mensajes"
    static let enviarMensaje = EndPoints.domain + "/chats/enviar-mensaje"
    static let videollamada = EndPoints.domain + "/chats/videollamada"
    
    static let especialidades = EndPoints.domain + "/red-medica/especialidades"
    static let proveedores = EndPoints.domain + "/red-medica/proveedores"
    
    static let citas = EndPoints.domain + "/citas/pendientes"
    static let medicosVisitados = EndPoints.domain + "/citas/medicos-visitados"
    static let medicosPorUbicacion = EndPoints.domain + "/citas/medicos-por-ubicacion"
    static let horariosMedico = EndPoints.domain + "/citas/horarios"
    static let reservarCita = EndPoints.domain + "/citas/reservar"
    static let confirmarCita = EndPoints.domain + "/citas/confirmar"
    static let anularCita = EndPoints.domain + "/citas/anular"
}
