//
//  Provider.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/30/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct Provider: Codable {
    
    let codigo: Int
    let descripcion: String
    let latitud: Double
    let longitud: Double
    let direccion: String
    let departamento: String
    let municipio: String
    let distancia: Double
    
}
