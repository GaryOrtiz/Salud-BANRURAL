//
//  CreateAppointment.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct CreateAppointment: Codable {
    
    var beneficiaryType:String
    var beneficiaryId:Int
    var beneficiaryName:String
    var symptoms:String
    var coverage:Int?
    var coverageName:String?
    var doctorCode:Int?
    var doctorName:String?
    var doctorAddress:String?
    var selectedDate:String?
    var selectedHour:String?
    var transaction:Int?
}
