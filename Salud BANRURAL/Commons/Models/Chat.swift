//
//  Chat.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct Chat: Codable {
    let id: Int
    let solicitante: String
    let operador: String
    let solicitante_user_id: Int?
    let avatar: String
    let texto_inicio: String
    let cola_id: Int?
    let cola: String
    let permite_videollamada: Int
    let estado: String
    let fecha: String
    let hora: String?
    let mensajes: [ChatMessage]?
}
