//
//  User.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct User: Codable {
    let id: Int
    let username: String
    let telefono: String
    let cambiar_password : Bool
    
}
