//
//  Beneficiary.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/5/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct Beneficiary: Codable {
    
    let BENEFICIARIO: Int
    let NOMBRE_BENEFICIARIO: String
    let APLICA_GINECOLOGIA: String
    let APLICA_PEDIATRIA: String
    
}
