//
//  Service.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/5/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct Service: Codable {
    
    let aplica_ginecologia: String
    let aplica_pediatria: String
    let beneficiarios: [Beneficiary]?
    let clasificacion_benef: String
    let contrato_fisico: String
    let fecha_inicio: String
    let ginecologia: String
    let medico_general: String
    let pediatria: String
    let plan: String
    let tiene_asesoria_medica: Int
    let titular: String
    
}
