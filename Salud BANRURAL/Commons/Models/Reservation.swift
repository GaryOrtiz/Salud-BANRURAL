//
//  ReservationResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct Reservation: Codable {
    let reservado: Bool
    let transaccion: Int
}
