//
//  ChatMessage.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ChatMessage: Codable {
    let id: Int
    let sala_chat_id: Int
    let fecha: String?
    let mensaje: String
    let envia_user_id: Int
    let envia: String
    let avatar: String
    let hora: String
    let ruta: String?
    let nombre_archivo: String?
    let tipo: String
}
