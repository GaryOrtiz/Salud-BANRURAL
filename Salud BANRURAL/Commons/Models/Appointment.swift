//
//  Appointment.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct Appointment: Codable {
    
    let codigo_cita: Int
    let nombre_medico: String
    let direccion: String
    let medico: Int
    let fecha: String
    let anio: Int
    let mes: Int
    let dia: Int
    let servicio: Int
    let paciente: String
    let nombre_cobertura: String
    
}
