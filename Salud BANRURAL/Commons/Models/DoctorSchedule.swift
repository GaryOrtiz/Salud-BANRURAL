//
//  DoctorSchedule.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct DoctorSchedule: Codable {
    
    let fecha: String
    let horarios:[DoctorScheduleHour]
    let horario_m: String
    let horario_t: String
    
}
