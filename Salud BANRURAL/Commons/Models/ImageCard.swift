//
//  ImageCard.swift
//  EPSS
//
//  Created by Informatica EPSS on 3/9/21.
//  Copyright © 2021 EPSS. All rights reserved.
//

import Foundation

struct ImageCard {
    let title: String
    let image: String
    let type: String
}
