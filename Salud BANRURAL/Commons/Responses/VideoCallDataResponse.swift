//
//  VideoCallDataResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/23/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct VideoCallDataResponse: Codable {
    
    let apiKey: String
    let sessionId: String
    let token: String
    let videollamadaId: Int
    
}
