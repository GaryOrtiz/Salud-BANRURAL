//
//  VideoCallResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/23/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct VideoCallResponse: Codable {
    
    let resultado: Bool
    let mensaje: String
    let datos: VideoCallDataResponse?
    
}
