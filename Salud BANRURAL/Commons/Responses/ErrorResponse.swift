//
//  ErrorResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ErrorResponse: Codable {
    let resultado: String
    let mensaje: String
}
