//
//  DoctorScheduleResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct DoctorScheduleResponse : Codable {
    
    let resultado: Bool
    let mensaje: String
    let datos: DoctorScheduleDataResponse?
    
}
