//
//  ConfirmAppointmentDataResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ConfirmAppointmentDataResponse: Codable {
    let codigo_cita: String
}
