//
//  DefaultResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/15/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct DefaultResponse: Codable {
    let resultado: Bool
    let mensaje: String
}
