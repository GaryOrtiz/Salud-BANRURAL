//
//  LoginResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct LoginResponse: Codable {
    
    let resultado: Bool
    let mensaje: String
    let datos: LoginDataResponse?
    
}
