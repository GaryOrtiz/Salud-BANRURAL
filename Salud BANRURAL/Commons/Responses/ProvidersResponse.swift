//
//  ProvidersResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/30/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ProvidersResponse: Codable {
    
    let resultado: Bool
    let mensaje: String
    let datos: ProvidersDataResponse?
    
}
