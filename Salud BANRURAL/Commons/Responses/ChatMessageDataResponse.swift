//
//  ChatMessageDataResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/23/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ChatMessageDataResponse: Codable {
    
    let mensaje: ChatMessage
    
}
