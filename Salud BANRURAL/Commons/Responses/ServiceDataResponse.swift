//
//  ServiceDataResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/5/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ServiceDataResponse: Codable {
    
    let contratos: Service
    
}
