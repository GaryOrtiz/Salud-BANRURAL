//
//  ReservationResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ReservationResponse: Codable {
    let resultado: Bool
    let mensaje: String
    let datos: ReservationDataResponse?
    let transaccion: Int
}
