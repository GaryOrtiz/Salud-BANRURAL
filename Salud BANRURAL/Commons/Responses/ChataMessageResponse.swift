//
//  ChataMessageResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/23/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ChatMessageResponse: Codable {
    
    let resultado: Bool
    let mensaje: String
    let datos: ChatMessageDataResponse?
    
}
