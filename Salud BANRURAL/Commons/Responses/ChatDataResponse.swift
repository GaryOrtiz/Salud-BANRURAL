//
//  ChatDataResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/9/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ChatDataResponse: Codable {
    
    let chat: Chat
    
}
