//
//  ServiceResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/5/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ServiceResponse : Codable {
    
    let resultado: Bool
    let mensaje: String
    let datos: ServiceDataResponse?
    
}
