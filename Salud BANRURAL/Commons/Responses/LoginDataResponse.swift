//
//  LoginDataResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct LoginDataResponse: Codable {
    let user: User
    let token: String
}
