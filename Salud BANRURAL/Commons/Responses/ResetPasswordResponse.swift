//
//  ResetPasswordResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/13/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ResetPasswordResponse: Codable {
    let resultado: Bool
    let mensaje: String
}
