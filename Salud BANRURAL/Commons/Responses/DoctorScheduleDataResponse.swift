//
//  DoctorScheduleDataResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct DoctorScheduleDataResponse: Codable {
    let horarios: [DoctorSchedule]
}
