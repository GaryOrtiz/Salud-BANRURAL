//
//  ConfirmAppointmentResponse.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ConfirmAppointmentResponse: Codable {
    let resultado: Bool
    let mensaje: String
    let datos: ConfirmAppointmentDataResponse?
}
