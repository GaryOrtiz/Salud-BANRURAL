//
//  OKAlertViewDelegate.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/8/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

protocol OkAlertViewDelegate: class {
    func okButtonTapped()
}
