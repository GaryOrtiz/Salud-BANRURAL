//
//  UIColorExtension.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/25/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

extension UIColor {
    
    /*BANRURAL*/
    static let primaryColor = UIColor(red:0/255,green:133/255,blue:63/255,alpha:1)
    static let secondaryColor = UIColor(red:255/255,green:131/255,blue:0/255,alpha:1)
    static let transparent = UIColor(red:255/255,green:255/255,blue:255/255,alpha:0.0)
    static let blackTransparentColor = UIColor(red:11/255,green:21/255,blue:33/255,alpha:0.7)
    static let whiteTransparentColor = UIColor(red:255/255,green:255/255,blue:255/255,alpha:0.5)
    
    static let darkBlue = UIColor(red:10/255,green:21/255,blue:33/255,alpha:1)
    static let lightGray = UIColor(red:240/255,green:240/255,blue:240/255,alpha:1)
    
    static let acceptButtonColor = UIColor.secondaryColor
    static let cancelButtonColor = UIColor.primaryColor
    static let cancelAppointmentButtonColor = UIColor.secondaryColor
    
    static let providerBackground = UIColor.white
    static let providerName = UIColor.primaryColor
    static let providerDistance = UIColor.darkBlue
    static let providerAddress = UIColor.secondaryColor
    static let providerState = UIColor.darkBlue
    
    static let providerBackgroundTint = UIColor.secondaryColor
    static let providerNameTint = UIColor.white
    static let providerDistanceTint = UIColor.white
    static let providerAddressTint = UIColor.primaryColor
    static let providerStateTint = UIColor.white
    
    static let bubbleReceivedMessage = UIColor(red: 210/255, green: 236/255, blue: 192/255, alpha: 1)
    static let textReceivedMessage = UIColor.darkBlue
    static let bubbleSentMessage = UIColor.white
    static let textSentMessage = UIColor.darkBlue
    static let tintDocumentMessage = UIColor.primaryColor
    
    static let profileTitle = UIColor.secondaryColor
    static let profileSubtitle = UIColor.primaryColor
}
