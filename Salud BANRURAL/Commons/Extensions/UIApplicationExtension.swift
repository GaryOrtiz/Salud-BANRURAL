//
//  UIApplicationExtension.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/9/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

extension UIApplication {
    func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        switch (base) {
        case let controller as UINavigationController:
            return topViewController(controller.visibleViewController)
        case let controller as UITabBarController:
            return controller.selectedViewController.flatMap { topViewController($0) } ?? base
        default:
            return base?.presentedViewController.flatMap { topViewController($0) } ?? base
        }
    }
}
