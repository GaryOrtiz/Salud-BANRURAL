//
//  UIViewControllerExtension.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/8/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import SCLAlertView

extension UIViewController {
    
    func presentErrorAlert2(message: String){
        let alert = UIAlertController(title: "Oops!", message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: nil))
        self.present(alert, animated: true)
    }
    
    func presentErrorAlert(message: String){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let showTimeOut = SCLButton.ShowTimeoutConfiguration()
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Aceptar", backgroundColor: .primaryColor, textColor: .white, showTimeout: showTimeOut) {}
        alertView.showError("¡Oops!", subTitle: message)
    }
    
    func presentSuccessAlert(title: String, message: String){
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let showTimeOut = SCLButton.ShowTimeoutConfiguration()
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Aceptar", backgroundColor: .primaryColor, textColor: .white, showTimeout: showTimeOut) {}
        alertView.showSuccess(title, subTitle: message)
    }
    
}
