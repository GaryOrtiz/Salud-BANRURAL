//
//  NewChatRequest.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/28/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct NewChatRequest: Codable {
    
    let texto_inicio: String
    
}
