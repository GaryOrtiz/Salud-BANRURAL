//
//  VideoCallRequest.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/23/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct VideoCallRequest: Codable {
    
    let sala_chat_id: Int
    
}
