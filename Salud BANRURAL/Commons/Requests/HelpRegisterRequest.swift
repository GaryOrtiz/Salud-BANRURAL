//
//  HelpRegisterRequest.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/14/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct HelpRegisterRequest: Codable {
    let nombre: String
    let correo: String
    let telefono: String
}
