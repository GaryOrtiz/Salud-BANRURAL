//
//  ConfirmAppointmentRequest.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ConfirmAppointmentRequest: Codable {
    
    let telefono: String
    let transaccion: Int
    let paciente: String
    let beneficiario: Int
    let cobertura: Int
    let sintomas: String
    
}
