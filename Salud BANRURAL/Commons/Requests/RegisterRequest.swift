//
//  RegisterRequest.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/15/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct RegisterRequest: Codable {
    let primer_nombre: String
    let segundo_nombre: String
    let primer_apellido: String
    let segundo_apellido: String
    let correo: String
    let telefono: String
    let username: String
    let password: String
    let password_confirmation: String
    let certificado: String
}
