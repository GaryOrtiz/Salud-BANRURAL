//
//  ReserveAppointment.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ReserveAppointmentRequest: Codable {
    
    let fecha: String
    let horario: String
    
}
