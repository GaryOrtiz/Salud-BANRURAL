//
//  ResetPasswordRequest.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/13/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import Foundation

struct ResetPasswordRequest: Codable {
    
    let username: String
    
}
