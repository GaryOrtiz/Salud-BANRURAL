//
//  RegisterViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/3/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD

class RegisterViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var secondNameTextField: UITextField!
    @IBOutlet weak var firstLastNameTextField: UITextField!
    @IBOutlet weak var secondLastNameTextField: UITextField!
    @IBOutlet weak var mailTextField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var certificateTextField: UITextField!
    
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var secondNameLabel: UILabel!
    @IBOutlet weak var firstLastNameLabel: UILabel!
    @IBOutlet weak var secondLastNameLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var confirmPasswordLabel: UILabel!
    @IBOutlet weak var certificateLabel: UILabel!
   
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var helpButton: UIButton!
    
    //MARK: - Actions
    @IBAction func registerAction() {
        view.endEditing(true)
        performRegister()
    }
    
    let spinner = JGProgressHUD(style: .dark)
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        view.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationController?.navigationBar.tintColor = .white
        
        logoImageView.image = UIImage(named: Globals.logo_white)
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 15
        registerButton.clipsToBounds = true
        registerButton.layer.cornerRadius = 15
        registerButton.backgroundColor = .secondaryColor
        phoneNumberTextField.keyboardType = .phonePad
        mailTextField.keyboardType = .emailAddress
        
        firstNameLabel.textColor = .primaryColor
        secondNameLabel.textColor = .primaryColor
        firstLastNameLabel.textColor = .primaryColor
        secondLastNameLabel.textColor = .primaryColor
        mailLabel.textColor = .primaryColor
        phoneNumberLabel.textColor = .primaryColor
        usernameLabel.textColor = .primaryColor
        passwordLabel.textColor = .primaryColor
        confirmPasswordLabel.textColor = .primaryColor
        certificateLabel.textColor = .primaryColor
        helpButton.setTitleColor(.primaryColor, for: .normal)
        
    }
    
    private func performRegister(){
        guard let firstName = firstNameTextField.text, !firstName.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu primer nombre")
            return
        }
        let secondName = secondNameTextField.text ?? ""
        guard let firstLastName = firstLastNameTextField.text, !firstLastName.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu primer apellido")
            return
        }
        let secondLastName = secondLastNameTextField.text ?? ""
        guard let email = mailTextField.text, !email.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu correo")
            return
        }
        guard let phoneNumber = phoneNumberTextField.text, !phoneNumber.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu teléfono")
            return
        }
        guard let username = usernameTextField.text, !username.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu usuario")
            return
        }
        guard let password = passwordTextField.text, !password.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu contraseña")
            return
        }
        guard let confirmPassword = confirmPasswordTextField.text, !confirmPassword.isEmpty else {
            self.presentErrorAlert(message: "Ingresa la confirmación de tu contraseña")
            return
        }
        guard password == confirmPassword else {
            self.presentErrorAlert(message: "Las contraseñas no coinciden")
            return
        }
        guard let certificate = certificateTextField.text, !certificate.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu certificado")
            return
        }
        
        
        self.spinner.show(in: self.view)
        
        let request = RegisterRequest(primer_nombre: firstName, segundo_nombre: secondName, primer_apellido: firstLastName, segundo_apellido: secondLastName, correo: email, telefono: phoneNumber, username: username, password: password, password_confirmation: confirmPassword, certificado: certificate)
        SN.post(endpoint: EndPoints.registrarse, model: request) { (response: SNResultWithEntity<DefaultResponse, ErrorResponse>) in
            
            print(response)
            self.spinner.dismiss()

            switch response {
            
            case .success(let result):
                if(result.resultado){
                    self.firstNameTextField.text = ""
                    self.secondNameTextField.text = ""
                    self.firstLastNameTextField.text = ""
                    self.secondLastNameTextField.text = ""
                    self.mailTextField.text = ""
                    self.phoneNumberTextField.text = ""
                    self.usernameTextField.text = ""
                    self.passwordTextField.text = ""
                    self.confirmPasswordTextField.text = ""
                    self.certificateTextField.text = ""
                    self.presentSuccessAlert(title: "Registro Completado", message: result.mensaje)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }

}
