//
//  ForgotPasswordViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/3/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD

class ForgotPasswordViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var userTextField: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var resetPasswordButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    
    //MARK: - Actions
    @IBAction func resetPasswordAction() {
        view.endEditing(true)
        performResetPassword()
    }
    
    let spinner = JGProgressHUD(style: .dark)
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        navigationController?.navigationBar.tintColor = .white
        view.backgroundColor = .primaryColor
        logoImageView.image = UIImage(named: Globals.logo_white)
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 15
        resetPasswordButton.clipsToBounds = true
        resetPasswordButton.layer.cornerRadius = 15
        resetPasswordButton.backgroundColor = .secondaryColor
        titleLabel.textColor = .primaryColor
        usernameLabel.textColor = .primaryColor
    }
    
    private func performResetPassword(){
        guard let username = userTextField.text, !username.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu usuario")
            return
        }
        
        self.spinner.show(in: self.view)
        
        let request = ResetPasswordRequest(username: username)
        SN.post(endpoint: EndPoints.reestablecerPassword, model: request) { (response: SNResultWithEntity<ResetPasswordResponse, ErrorResponse>) in
            
            self.spinner.dismiss()

            switch response {
            
            case .success(let result):
                if(result.resultado){
                    self.userTextField.text = ""
                    self.presentSuccessAlert(title: "Contraseña reestablecida", message: result.mensaje)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }

}
