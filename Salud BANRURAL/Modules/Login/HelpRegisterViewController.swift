//
//  HelpRegisterViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/14/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD

class HelpRegisterViewController: UIViewController {

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var getHelpButton: UIButton!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var mailLabel: UILabel!
    @IBOutlet weak var phoneNumberLabel: UILabel!
    
    //MARK: - Actions
    @IBAction func getHelpAction() {
        view.endEditing(true)
        performGetHelpRegister()
    }
    
    let spinner = JGProgressHUD(style: .dark)
    
    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func setupUI(){
        view.backgroundColor = .primaryColor
        navigationController?.navigationBar.tintColor = .white
        logoImageView.image = UIImage(named: Globals.logo_white)
        mainView.clipsToBounds = true
        mainView.layer.cornerRadius = 15
        getHelpButton.clipsToBounds = true
        getHelpButton.layer.cornerRadius = 15
        getHelpButton.backgroundColor = .secondaryColor
        phoneTextField.keyboardType = .phonePad
        
        titleLabel.textColor = .primaryColor
        nameLabel.textColor = .primaryColor
        mailLabel.textColor = .primaryColor
        phoneNumberLabel.textColor = .primaryColor
        
    }
    
    private func performGetHelpRegister(){
        guard let name = nameTextField.text, !name.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu nombre")
            return
        }
        guard let email = emailTextField.text, !email.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu correo")
            return
        }
        guard let phone = phoneTextField.text, !phone.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu teléfono")
            return
        }
        
        self.spinner.show(in: self.view)
        
        let request = HelpRegisterRequest(nombre: name, correo: email, telefono: phone)
        SN.post(endpoint: EndPoints.ayudaRegistro, model: request) { (response: SNResultWithEntity<ResetPasswordResponse, ErrorResponse>) in
            
            self.spinner.dismiss()

            switch response {
            
            case .success(let result):
                if(result.resultado){
                    self.nameTextField.text = ""
                    self.phoneTextField.text = ""
                    self.emailTextField.text = ""
                    self.presentSuccessAlert(title: "Solicitar ayuda", message: result.mensaje)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }

}
