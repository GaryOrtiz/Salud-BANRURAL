//
//  LoginViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/3/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
//import OneSignal
import KeychainSwift

class LoginViewController: UIViewController {
    
    //MARK: - Outlets
    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var loginCard: UIView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!
    @IBOutlet weak var usernameText: UITextField!
    @IBOutlet weak var passwordText: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var forgotPasswordButton: UIButton!
    @IBOutlet weak var loginMessageView: UIView!
    
    //MARK: - Actions
    @IBAction func loginAction() {
        view.endEditing(true)
        performLogin()
    }
    
    //MARK: - Properties
    let spinner = JGProgressHUD(style: .dark)

    override func viewWillDisappear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = false
        navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        navigationController?.setNavigationBarHidden(true, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI()
    {
        view.backgroundColor = .primaryColor
        logoImageView.image = UIImage(named: Globals.logo_white)
        loginButton.backgroundColor = .secondaryColor
        loginButton.layer.cornerRadius = 15
        loginCard.layer.cornerRadius = 15
        usernameText.text = ""
        passwordText.text = ""
        usernameLabel.textColor = .primaryColor
        passwordLabel.textColor = .primaryColor
        registerButton.setTitleColor(.primaryColor, for: .normal)
        forgotPasswordButton.setTitleColor(.primaryColor, for: .normal)
        loginMessageView.clipsToBounds = true
        loginMessageView.layer.cornerRadius = 15
    }
    
    private func performLogin(){
        guard let username = usernameText.text, !username.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu usuario")
            return
        }
        guard let password = passwordText.text, !password.isEmpty else {
            self.presentErrorAlert(message: "Ingresa tu contraseña")
            return
        }
        
        
        self.spinner.show(in: self.view)

        let request = LoginRequest(username: username, password: password)
        SN.post(endpoint: EndPoints.login, model: request) { (response: SNResultWithEntity<LoginResponse, ErrorResponse>) in
            print(response)
            self.spinner.dismiss()

            switch response {
            
            case .success(let user):
                if(user.resultado){
                    guard let data = user.datos else {
                        return
                    }
                    let keychain = KeychainSwift()
                    keychain.set(data.token, forKey: "token")
                    SimpleNetworking.setAuthenticationHeader(prefix: "bearer", token: data.token)
//                    OneSignal.setExternalUserId("epss.ios.\(data.user.id)")
                    let encoder = JSONEncoder()
                    if let encoded = try? encoder.encode(data.user) {
                        UserDefaults.standard.set(encoded, forKey: "user")
                    }
//                    self.navigationController?.popViewController(animated: true)
                    let storyboard = UIStoryboard(name: "Home", bundle: nil)
                    if let homeVC = storyboard.instantiateViewController(withIdentifier: "homeViewControllerID") as? HomeTabBarViewController {
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = homeVC
                    }
                    
                }
                else{
                    self.presentErrorAlert(message: user.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }

}
