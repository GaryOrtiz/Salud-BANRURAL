//
//  CentersViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/30/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import CoreLocation

class CentersViewController: UIViewController {
    
    //MARK: - Properties
    private let cellId = "ProviderTableViewCell"
    private var dataSource = [Provider]()
    let spinner = JGProgressHUD(style: .dark)
    var distance: Float = 10
    var centerType = ""
    private var locationManager: CLLocationManager?
    private var userLocation: CLLocation?

    fileprivate let slider: UISlider = {
        let slider = UISlider()
        slider.tintColor = .secondaryColor
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        return slider
    }()
    
    fileprivate let kmLabel: UILabel = {
       let label = UILabel()
        label.text = "Buscar proveedor 10.0 km a la redonda"
        label.textColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let searchButton: UIButton = {
       let button = UIButton()
        button.setTitle("Consultar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .secondaryColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(getProviders), for: .touchUpInside)
        return button
    }()
    
    fileprivate let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        requestLocation()
    }
    
    private func setupUI(){
        navigationController?.navigationBar.tintColor = .white
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        view.backgroundColor = .white
        
        slider.minimumValue = 5
        slider.maximumValue = 50
        slider.value = 10
        
        view.addSubview(kmLabel)
        view.addSubview(slider)
        view.addSubview(searchButton)
        view.addSubview(tableView)
        
        kmLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        kmLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        kmLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        
        slider.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        slider.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        slider.topAnchor.constraint(equalTo: kmLabel.bottomAnchor, constant: 10).isActive = true
        
        searchButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        searchButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        searchButton.topAnchor.constraint(equalTo: slider.bottomAnchor, constant: 20).isActive = true
        searchButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        tableView.topAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 20).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
 
    }
    
    @objc func sliderValueChanged(){
        let steps = Float(5)
        slider.value = roundf(slider.value/slider.maximumValue*steps)*slider.maximumValue/steps;
        kmLabel.text = "Buscar proveedor \(slider.value) km a la redonda"
        distance = slider.value
    }
    
    private func requestLocation(){
        //validar si tiene gps activo y disponible
        guard CLLocationManager.locationServicesEnabled() else {
            return
        }
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        
    }
    
    @objc func getProviders(){
        guard let userLocation = userLocation else {
            presentErrorAlert(message: "No se pudo obtener su ubicación.")
            return
        }
        
        self.spinner.show(in: self.view)
        let lat = userLocation.coordinate.latitude
        let lon = userLocation.coordinate.longitude
        let endpoint = "\(EndPoints.proveedores)/C/\(centerType)/\(lat)/\(lon)/\(distance)"
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<ProvidersResponse, ErrorResponse>) in
            
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    self.dataSource = data.proveedores
                    self.tableView.reloadData()
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }
}

//MARK: - UITableViewDataSource
extension CentersViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath)

        if let cell = cell as? ProviderTableViewCell{
            cell.setupCellWith(provider: dataSource[indexPath.row], position: indexPath.row)
        }
        
        return cell
    }
    
}

extension CentersViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? ProviderTableViewCell else {
            return
        }
        
        if(indexPath.row % 2 == 0){
            cell.backgroundColor = .providerBackground
            cell.nameLabel.textColor = .providerName
            cell.distanceLabel.textColor = .providerDistance
            cell.addressLabel.textColor = .providerAddress
            cell.countryLabel.textColor = .providerState
        }
        else{
            cell.backgroundColor = .providerBackgroundTint
            cell.nameLabel.textColor = .providerNameTint
            cell.distanceLabel.textColor = .providerDistanceTint
            cell.addressLabel.textColor = .providerAddressTint
            cell.countryLabel.textColor = .providerStateTint
        }
    }
}

extension CentersViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let bestLocation = locations.last else {
            return
        }
        userLocation = bestLocation
    }
}
