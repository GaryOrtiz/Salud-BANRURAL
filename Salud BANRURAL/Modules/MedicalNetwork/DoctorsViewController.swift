//
//  DoctorsTableViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/30/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import CoreLocation

class DoctorsViewController: UIViewController {

    
    //MARK: - Properties
    let spinner = JGProgressHUD(style: .dark)
    private var specialty: Specialty?
    private var dataSource = [Provider]()
    private let cellId = "ProviderTableViewCell"
    private var userLocation: CLLocation?
    private var distance: Float = 0
    
    fileprivate let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    /*fileprivate let closeButton: UIButton = {
        
    }()*/
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .white
        navigationController?.navigationBar.tintColor = .white
        
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        
        view.addSubview(tableView)
        
        tableView.topAnchor.constraint(equalTo: view.topAnchor, constant: 20).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        
        getProviders()
    }
    
    init(with specialty: Specialty, userLocation: CLLocation?, distance: Float) {
        super.init(nibName: nil, bundle: nil)
        self.specialty = specialty
        self.userLocation = userLocation
        self.distance = distance
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func getProviders(){
        guard let userLocation = userLocation else {
            presentErrorAlert(message: "No se pudo obtener su ubicación.")
            return
        }
        guard let specialtyCode = specialty?.codigo else {
            return
        }
        
        self.spinner.show(in: self.view)
        let lat = userLocation.coordinate.latitude
        let lon = userLocation.coordinate.longitude
        let endpoint = "\(EndPoints.proveedores)/M/\(specialtyCode)/\(lat)/\(lon)/\(distance)"
        
        print(endpoint)
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<ProvidersResponse, ErrorResponse>) in
            
            self.spinner.dismiss()
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    self.dataSource = data.proveedores
                    self.tableView.reloadData()
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
        }
    }
}

//MARK: - UITableViewDataSource
extension DoctorsViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath)

        if let cell = cell as? ProviderTableViewCell{
            cell.setupCellWith(provider: dataSource[indexPath.row], position: indexPath.row)
        }

        return cell
    }
    
}

extension DoctorsViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? ProviderTableViewCell else {
            return
        }
        
        if(indexPath.row % 2 == 0){
            cell.backgroundColor = .providerBackground
            cell.nameLabel.textColor = .providerName
            cell.distanceLabel.textColor = .providerDistance
            cell.addressLabel.textColor = .providerAddress
            cell.countryLabel.textColor = .providerState
        }
        else{
            cell.backgroundColor = .providerBackgroundTint
            cell.nameLabel.textColor = .providerNameTint
            cell.distanceLabel.textColor = .providerDistanceTint
            cell.addressLabel.textColor = .providerAddressTint
            cell.countryLabel.textColor = .providerStateTint
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
