//
//  SpecialtiesViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/30/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import CoreLocation

class SpecialtiesViewController: UIViewController {

    fileprivate let slider: UISlider = {
        let slider = UISlider()
        slider.tintColor = .secondaryColor
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        return slider
    }()
    
    fileprivate let kmLabel: UILabel = {
       let label = UILabel()
        label.text = "Buscar médicos 10.0 km a la redonda"
        label.textColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let searchButton: UIButton = {
       let button = UIButton()
        button.setTitle("Consultar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .secondaryColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(getSpecialties), for: .touchUpInside)
        return button
    }()
    
    fileprivate let specialtiesTableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    @objc func sliderValueChanged(_ sender: Any) {
        let steps = Float(5)
        slider.value = roundf(slider.value/slider.maximumValue*steps)*slider.maximumValue/steps;
        kmLabel.text = "Buscar médicos \(slider.value) km a la redonda"
        distance = slider.value
    }
    
    //MARK: - Properties
    private let cellId = "SpecialtyTableViewCell"
    private var dataSource = [Specialty]()
    let spinner = JGProgressHUD(style: .dark)
    var distance: Float = 10
    private var locationManager: CLLocationManager?
    private var userLocation: CLLocation?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        requestLocation()
    }
    
    private func setupUI(){
        view.backgroundColor = .white
        navigationController?.navigationBar.tintColor = .white
        kmLabel.textColor = .primaryColor
        kmLabel.text = "Buscar médicos 10.0 km a la redonda"
        
        slider.minimumValue = 5
        slider.maximumValue = 50
        slider.value = 10
        
        view.addSubview(kmLabel)
        view.addSubview(slider)
        view.addSubview(searchButton)
        view.addSubview(specialtiesTableView)
        
        kmLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 15).isActive = true
        kmLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -15).isActive = true
        kmLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        
        slider.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        slider.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        slider.topAnchor.constraint(equalTo: kmLabel.bottomAnchor, constant: 10).isActive = true
        
        searchButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        searchButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        searchButton.topAnchor.constraint(equalTo: slider.bottomAnchor, constant: 20).isActive = true
        searchButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        specialtiesTableView.topAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 20).isActive = true
        specialtiesTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        specialtiesTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        specialtiesTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
        
        //TABLE VIEW
        specialtiesTableView.dataSource = self
        specialtiesTableView.delegate = self
        specialtiesTableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
    }

    @objc private func getSpecialties(){
        guard let userLocation = userLocation else {
            presentErrorAlert(message: "No se pudo obtener su ubicación.")
            return
        }
        
        self.spinner.show(in: self.view)
        let lat = userLocation.coordinate.latitude
        let lon = userLocation.coordinate.longitude
        let endpoint = "\(EndPoints.especialidades)/M/\(lat)/\(lon)/\(distance)"
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<SpecialtiesResponse, ErrorResponse>) in
            
            self.spinner.dismiss()
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    self.dataSource = data.especialidades
                    self.specialtiesTableView.reloadData()
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }
    
    private func requestLocation(){
        //validar si tiene gps activo y disponible
        guard CLLocationManager.locationServicesEnabled() else {
            return
        }
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        
    }
}

//MARK: - UITableViewDataSource
extension SpecialtiesViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath)

        if let cell = cell as? SpecialtyTableViewCell{
            cell.setupCellWith(specialty: dataSource[indexPath.row])
        }
        
        return cell
    }
    
}

extension SpecialtiesViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let specialty = dataSource[indexPath.row]
        let vc = DoctorsViewController(with: specialty, userLocation: userLocation, distance: distance)
        vc.title = specialty.descripcion
        navigationController?.pushViewController(vc, animated: true)
        
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? SpecialtyTableViewCell else {
            return
        }
        
        if(indexPath.row % 2 == 0){
            cell.backgroundColor = .white
            cell.specialtyLabel.textColor = .primaryColor
            cell.quantityLabel.textColor = .white
            cell.quantityLabel.backgroundColor = .primaryColor
        }
        else{
            cell.backgroundColor = .primaryColor
            cell.specialtyLabel.textColor = .white
            cell.quantityLabel.textColor = .primaryColor
            cell.quantityLabel.backgroundColor = .white
        }
    }
}

extension SpecialtiesViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let bestLocation = locations.last else {
            return
        }
        userLocation = bestLocation
    }
}
