//
//  ProviderTableViewCell.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/30/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class ProviderTableViewCell: UITableViewCell {

    @IBOutlet weak var stack: UIStackView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCellWith(provider: Provider, position: Int){
        nameLabel.text = provider.descripcion
        distanceLabel.text = "Distancia: \(provider.distancia) KM"
        addressLabel.text = provider.direccion
        countryLabel.text = "\(provider.departamento), \(provider.municipio)"
    }
}
