//
//  SpecialtyTableViewCell.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/30/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class SpecialtyTableViewCell: UITableViewCell {

    @IBOutlet weak var specialtyLabel: UILabel!
    @IBOutlet weak var quantityLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCellWith(specialty: Specialty){
        specialtyLabel.text = specialty.descripcion
        quantityLabel.text = "\(specialty.cantidad)"
        
        quantityLabel.clipsToBounds = true
        quantityLabel.layer.cornerRadius = 15
    }
    
}
