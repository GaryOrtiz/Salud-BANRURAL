//
//  MedicalNetworkViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/29/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class MedicalNetworkViewController: UIViewController {
    
    let cellId = "ImageCardTableViewCell"
    var dataSource = [ImageCard]()
    
    fileprivate let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupUI()
    }
    
    public func setupUI() {
        navigationController?.navigationBar.tintColor = .white
        let medicalAssistanceCard = ImageCard(title: "Médicos", image: Globals.img_doctor, type: "doctors")
        let makeAppointmentCard = ImageCard(title: "Hospitales", image: Globals.img_hospital, type: "hospitals")
        let chatWithUsCard = ImageCard(title: "Laboratorios", image: Globals.img_laboratory, type: "laboratories")
        let medicalNetworkCard = ImageCard(title: "Centros de diagnóstico", image: Globals.img_diagnostic_center, type: "diagnostic_centers")
        
        dataSource.append(medicalAssistanceCard)
        dataSource.append(makeAppointmentCard)
        dataSource.append(chatWithUsCard)
        dataSource.append(medicalNetworkCard)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        
        view.addSubview(tableView)
        
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
    }
    
    @objc func goToHome(){
        self.dismiss(animated: true, completion: nil)
    }
    
    func showDoctors(){
        let vc = SpecialtiesViewController()
        vc.title = "Especialidades"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showHospitals(){
        let vc = CentersViewController()
        vc.title = "Hospitales"
        vc.centerType = "H"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showLaboratories(){
        let vc = CentersViewController()
        vc.title = "Laboratorios"
        vc.centerType = "L"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func showDiagnosticCenters(){
        let vc = CentersViewController()
        vc.title = "Centros de diagnóstico"
        vc.centerType = "D"
        navigationController?.pushViewController(vc, animated: true)
    }

}

//MARK: - UITableViewDataSource
extension MedicalNetworkViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath)

        if let cell = cell as? ImageCardTableViewCell{
            cell.setupCellWith(imageCard: dataSource[indexPath.row])
        }
        
        return cell
    }
    
}

extension MedicalNetworkViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let card = dataSource[indexPath.row]
        if card.type == "doctors" {
            showDoctors()
        }
        else if card.type == "hospitals" {
            showHospitals()
        }
        else if card.type == "laboratories" {
            showLaboratories()
        }
        else if card.type == "diagnostic_centers" {
            showDiagnosticCenters()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? ImageCardTableViewCell else {
            return
        }
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.stackView.clipsToBounds = true
        cell.stackView.layer.cornerRadius = 15
        cell.titleLabel.backgroundColor = .primaryColor
        cell.titleLabel.textColor = .white
        
    }
}
