//
//  AppointmentsTableViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import SCLAlertView

class AppointmentsTableViewController: UITableViewController {

    private var appointments = [Appointment]()
    let spinner = JGProgressHUD(style: .dark)
    let cellId = "AppointmentTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableView.separatorStyle = .none
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        getAppointments()
    }
    
    private func setupUI() {
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
    }
    
    private func getAppointments(){
        self.spinner.show(in: self.view)
        let endpoint = "\(EndPoints.citas)"
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<AppointmentsResponse, ErrorResponse>) in
            print(response)
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    self.appointments = data.citas
                    if (self.appointments.isEmpty){
                        self.presentErrorAlert2(message: "No tiene citas pendientes!")
                    }
                    self.tableView.reloadData()
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return appointments.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath)

        if let cell = cell as? AppointmentTableViewCell{
            cell.setupCellWith(appointment: appointments[indexPath.row])
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? AppointmentTableViewCell else {
            return
        }
        
        cell.cancelButton.tag = indexPath.row
        cell.cancelButton.addTarget(self, action: #selector(cancelAppointmentAction), for: .touchUpInside)
        
        cell.stackView.clipsToBounds = true
        cell.stackView.layer.cornerRadius = 15
        cell.doctorAddressLabel.numberOfLines = 2
        
    }
    
    @objc func cancelAppointmentAction(sender: UIButton){
        let index = sender.tag
        let appointment = appointments[index]
        /*Alert**/
        let appearance = SCLAlertView.SCLAppearance(
            showCloseButton: false
        )
        let showTimeOut = SCLButton.ShowTimeoutConfiguration()
        let alertView = SCLAlertView(appearance: appearance)
        alertView.addButton("Anular", backgroundColor: .primaryColor, textColor: .white, showTimeout: showTimeOut) {
            self.cancelAppointment(appointment: appointment)
        }
        alertView.addButton("Cancelar", backgroundColor: .red, textColor: .white, showTimeout: showTimeOut){
            
        }
        alertView.showNotice("Anular cita", subTitle: "¿Seguro que desea anular la cita \(appointment.codigo_cita)")
    }
    
    func cancelAppointment(appointment: Appointment){
        self.spinner.show(in: self.view)
        let endpoint = "\(EndPoints.anularCita)"
        
        SN.post(endpoint: endpoint , model: appointment) { ( response: SNResultWithEntity<CancelAppointmentResponse, ErrorResponse>) in
            print(response)
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    /*Alert**/
                    /*let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let showTimeOut = SCLButton.ShowTimeoutConfiguration()
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Aceptar", backgroundColor: .primaryColor, textColor: .white, showTimeout: showTimeOut) {
                        self.getAppointments()
                    }
                    alertView.showSuccess("Anular cita", subTitle: result.mensaje)*/
                    let alert = AlertView()
                    alert.okButton.addTarget(self, action: #selector(self.refreshAppointments), for: .touchUpInside)
                    alert.show(title: "Anular cita", subTitle: result.mensaje, type: .success, iconColor: .secondaryColor, titleColor: .primaryColor, buttonColor: .secondaryColor)
                    self.view.addSubview(alert)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }
    
    @objc func refreshAppointments() {
        getAppointments()
    }

}
