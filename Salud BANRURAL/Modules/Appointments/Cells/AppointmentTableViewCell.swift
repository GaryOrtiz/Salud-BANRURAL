//
//  AppointmentTableViewCell.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class AppointmentTableViewCell: UITableViewCell {
    
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var dateTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var patientTitleLabel: UILabel!
    @IBOutlet weak var patientLabel: UILabel!
    @IBOutlet weak var doctorTitleLabel: UILabel!
    @IBOutlet weak var doctorLabel: UILabel!
    @IBOutlet weak var doctorAddressLabel: UILabel!
    @IBOutlet weak var specialtyTitleLabel: UILabel!
    @IBOutlet weak var specialtyLabel: UILabel!
    @IBOutlet weak var codeTitleLabel: UILabel!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var cancelButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setupCellWith(appointment: Appointment){
        dateLabel.text = appointment.fecha
        patientLabel.text = appointment.paciente
        doctorLabel.text = appointment.nombre_medico
        doctorAddressLabel.text = appointment.direccion
        specialtyLabel.text = appointment.nombre_cobertura
        codeLabel.text = "\(appointment.codigo_cita)"
        
        dateTitleLabel.backgroundColor = .primaryColor
        dateTitleLabel.textColor = .white
        dateLabel.backgroundColor = .white
        dateLabel.textColor = .primaryColor
        
        patientTitleLabel.backgroundColor = .primaryColor
        patientTitleLabel.textColor = .white
        patientLabel.backgroundColor = .white
        patientLabel.textColor = .primaryColor
        
        doctorTitleLabel.backgroundColor = .primaryColor
        doctorTitleLabel.textColor = .white
        doctorLabel.backgroundColor = .white
        doctorLabel.textColor = .primaryColor
        doctorAddressLabel.backgroundColor = .white
        doctorAddressLabel.textColor = .primaryColor
        
        specialtyTitleLabel.backgroundColor = .primaryColor
        specialtyTitleLabel.textColor = .white
        specialtyLabel.backgroundColor = .white
        specialtyLabel.textColor = .primaryColor
        
        codeTitleLabel.backgroundColor = .primaryColor
        codeTitleLabel.textColor = .white
        codeLabel.backgroundColor = .white
        codeLabel.textColor = .primaryColor
        
        cancelButton.backgroundColor = .cancelAppointmentButtonColor
        
    }
    
}
