//
//  CASelectBeneficiaryViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/4/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD

class CASelectBeneficiaryViewController: UIViewController {

    var types =  [String]()
    var beneficiaries =  [Beneficiary]()
    let spinner = JGProgressHUD(style: .dark)
    var service: Service?
    var beneficiaryType:String = "S"
    var beneficiaryId:Int = -1
    var beneficiaryName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getBeneficiaries()
    }
    
    fileprivate let stack: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 10
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    fileprivate let forWhoLabel: UILabel = {
       let label = UILabel()
        label.text = "¿Para quién desea la cita?"
        label.textColor = .primaryColor
        label.font = label.font.withSize(25)
        label.numberOfLines = 3
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let beneficiaryTypePicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    fileprivate let beneficiaryTypeText: UITextField = {
        let text = UITextField()
        text.text = "Seleccione"
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.primaryColor.cgColor
        text.layer.cornerRadius = 5
        text.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: text.frame.height))
        text.leftViewMode = .always
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    fileprivate let beneficiariesPicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    fileprivate let beneficiaryText: UITextField = {
        let text = UITextField()
        text.text = "Selecciona un beneficiario"
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.primaryColor.cgColor
        text.layer.cornerRadius = 5
        text.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: text.frame.height))
        text.leftViewMode = .always
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    fileprivate let symptomsText: UITextField = {
        let text = UITextField()
        text.placeholder = "Ej. Fiebre, dolor de cabeza, etc..."
        text.text = ""
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.primaryColor.cgColor
        text.layer.cornerRadius = 5
        text.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: text.frame.height))
        text.leftViewMode = .always
        text.translatesAutoresizingMaskIntoConstraints = false
        
        return text
    }()
    
    fileprivate let gotoStep2Button: UIButton = {
        let button = UIButton()
        button.setTitle("Siguiente", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .acceptButtonColor
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(goToStep2), for: .touchUpInside)
        
        return button
    }()
    
    
    
    private func setupUI() {
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        navigationController?.navigationBar.tintColor = .white
        
        types.append("Seleccione")
        types.append("Titular")
        
        beneficiaryTypePicker.delegate = self
        beneficiaryTypePicker.dataSource = self
        beneficiariesPicker.delegate = self
        beneficiariesPicker.dataSource = self
        
        view.backgroundColor = .white
        beneficiaryTypeText.inputView = beneficiaryTypePicker
        beneficiaryText.inputView = beneficiariesPicker
        
        view.addSubview(stack)
        
        stack.addArrangedSubview(forWhoLabel)
        stack.addArrangedSubview(beneficiaryTypeText)
        stack.addArrangedSubview(beneficiaryText)
        stack.addArrangedSubview(symptomsText)
        stack.addArrangedSubview(gotoStep2Button)
        
        stack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        
        beneficiaryTypeText.heightAnchor.constraint(equalToConstant: 50).isActive = true
        beneficiaryText.heightAnchor.constraint(equalToConstant: 50).isActive = true
        symptomsText.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        gotoStep2Button.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        gotoStep2Button.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        gotoStep2Button.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        beneficiaryText.isHidden = true
        
    }
    
    private func getBeneficiaries(){
        self.spinner.show(in: self.view)
        
        let decoded = UserDefaults.standard.data(forKey: "user")
        let decoder = JSONDecoder()
        guard let data = decoded, let user = try? decoder.decode(User.self, from: data) else {
            presentErrorAlert(message: "No se pudo obtener el usuario.")
            return
        }
        let endpoint = "\(EndPoints.contrato)"
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<ServiceResponse, ErrorResponse>) in
            
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    guard var beneficiariesData = data.contratos.beneficiarios else {
                        return
                    }
                    self.service = data.contratos
                    if(!beneficiariesData.isEmpty){
                        self.types.append("Beneficiario")
                        self.beneficiaryTypePicker.reloadAllComponents()
                        beneficiariesData.insert(Beneficiary(BENEFICIARIO: 0, NOMBRE_BENEFICIARIO: "Selecciona un beneficiario", APLICA_GINECOLOGIA: "N", APLICA_PEDIATRIA: "N"), at: 0)
                        self.beneficiaries = beneficiariesData
                        self.beneficiariesPicker.reloadAllComponents()
                    }
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }
    
    @objc func goToStep2(){
        if beneficiaryType == "S" {
            presentErrorAlert(message: "Selecciona para quien desea la cita")
            return
        }
        if beneficiaryType == "B" && beneficiaryId == 0 {
            presentErrorAlert(message: "Selecciona un beneficiario")
            return
        }
        guard let symptoms = symptomsText.text,
              !symptoms.isEmpty else {
            presentErrorAlert(message: "Ingresa tus síntomas por favor")
            return
        }
        guard let service = service else {
            return
        }
        let createAppointment = CreateAppointment(beneficiaryType: beneficiaryType, beneficiaryId: beneficiaryId, beneficiaryName: beneficiaryName, symptoms: symptoms)
        let vc = CASelectCoverageViewController(with: service, createAppointment: createAppointment)
        vc.title = "Especialidad"
        navigationController?.pushViewController(vc, animated: true)
    }

}

extension CASelectBeneficiaryViewController: UIPickerViewDelegate {
     
    // Sets the number of rows in the picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == beneficiaryTypePicker {
            return types.count
        }
        else {
            return beneficiaries.count
        }
    }
 
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if pickerView == beneficiaryTypePicker {
            beneficiaryTypeText.text = types[row]
            
            if(beneficiaryTypeText.text == "Titular"){
                beneficiaryType = "T"
                beneficiaryId = 0
                if let beneficiaryName = service?.titular {
                    self.beneficiaryName = beneficiaryName
                }
                beneficiaryText.isHidden = true
            }
            else if(beneficiaryTypeText.text == "Beneficiario"){
                beneficiaryType = "B"
                beneficiaryText.isHidden = false
            }
            else {
                beneficiaryType = "S"
                beneficiaryId = -1
                beneficiaryText.isHidden = true
            }
        }
        else {
            beneficiaryText.text = beneficiaries[row].NOMBRE_BENEFICIARIO
            beneficiaryId = beneficiaries[row].BENEFICIARIO
            beneficiaryName = beneficiaries[row].NOMBRE_BENEFICIARIO
        }
    }
    
}

extension  CASelectBeneficiaryViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if pickerView == beneficiaryTypePicker {
            return types[row]
        }
        else {
            return beneficiaries[row].NOMBRE_BENEFICIARIO
        }
    }
    
}

