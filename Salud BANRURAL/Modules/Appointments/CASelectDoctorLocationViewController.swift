//
//  CASelectDoctorLocationViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class CASelectDoctorLocationViewController: UIViewController {

    var service: Service?
    var createAppointment: CreateAppointment?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    init(with service: Service, createAppointment: CreateAppointment) {
        super.init(nibName: nil, bundle: nil)
        self.service = service
        self.createAppointment = createAppointment
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate let scrollView: UIScrollView = {
        
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
        
    }()
    
    fileprivate let stackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 10
        stack.distribution = .fill
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    fileprivate let recentDoctorsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.clipsToBounds = true
        stack.layer.cornerRadius = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    fileprivate let recentDoctorsLabel: UILabel = {
        let label = UILabel()
        label.text = "Médico visitados"
        label.backgroundColor = .primaryColor
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let recentDoctorsImage: UIImageView = {
        let image = UIImage(named: "medico_general")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    fileprivate let locationDoctorsStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.clipsToBounds = true
        stack.layer.cornerRadius = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    fileprivate let locationDoctorsLabel: UILabel = {
        let label = UILabel()
        label.text = "Médicos cercanos"
        label.backgroundColor = .primaryColor
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let locationDoctorsImage: UIImageView = {
        let image = UIImage(named: "red_medica")
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()
    
    private func setupUI(){
        view.backgroundColor = .white
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        navigationController?.navigationBar.tintColor = .white
        
        view.addSubview(scrollView)
        
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        scrollView.addSubview(stackView)
        
        stackView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor, constant: 10).isActive = true
        stackView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor, constant: 10).isActive = true
        stackView.widthAnchor.constraint(equalTo: scrollView.frameLayoutGuide.widthAnchor).isActive = true
        
        recentDoctorsLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        locationDoctorsLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        recentDoctorsStackView.addArrangedSubview(recentDoctorsLabel)
        recentDoctorsStackView.addArrangedSubview(recentDoctorsImage)
        stackView.addArrangedSubview(recentDoctorsStackView)
        
        locationDoctorsStackView.addArrangedSubview(locationDoctorsLabel)
        locationDoctorsStackView.addArrangedSubview(locationDoctorsImage)
        stackView.addArrangedSubview(locationDoctorsStackView)
        
        recentDoctorsStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        recentDoctorsStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        locationDoctorsStackView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 10).isActive = true
        locationDoctorsStackView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -10).isActive = true
        
        recentDoctorsStackView.isHidden = true
        
        recentDoctorsStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectRecentDoctors)))
        locationDoctorsStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectLocationDoctors)))
    }
    
    @objc func selectRecentDoctors(){
        
    }
    
    @objc func selectLocationDoctors(){
        guard let service = service, let createAppintment = createAppointment else {
            return
        }
        let vc = CASelectDoctorByLocationViewController(with: service, createAppointment: createAppintment)
        vc.title = "Médicos"
        navigationController?.pushViewController(vc, animated: true)
    }
    
}
