//
//  CASelectCoverageViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/5/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class CASelectCoverageViewController: UIViewController {

    var service: Service?
    var createAppointment: CreateAppointment?
    
    init(with service: Service, createAppointment: CreateAppointment) {
        super.init(nibName: nil, bundle: nil)
        self.service = service
        self.createAppointment = createAppointment
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    fileprivate let scrollView: UIScrollView = {
        
        let scroll = UIScrollView()
        scroll.translatesAutoresizingMaskIntoConstraints = false
        return scroll
        
    }()
    
    fileprivate let stackView: UIStackView = {
        let stack = UIStackView()
        stack.spacing = 10
        stack.axis = .vertical
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    fileprivate let generalStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .fill
        stack.clipsToBounds = true
        stack.layer.cornerRadius = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    fileprivate let generalLabel: UILabel = {
        let label = UILabel()
        label.text = "Médico general"
        label.backgroundColor = .primaryColor
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let generalImage: UIImageView = {
        let image = UIImage(named: Globals.img_general_medicine)
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    fileprivate let gineStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.clipsToBounds = true
        stack.layer.cornerRadius = 15
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    fileprivate let gineLabel: UILabel = {
        let label = UILabel()
        label.text = "Ginécologo"
        label.backgroundColor = .primaryColor
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let gineImage: UIImageView = {
        let image = UIImage(named: Globals.img_gynecology)
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleToFill
        return imageView
    }()
    
    fileprivate let pediaStackView: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.clipsToBounds = true
        stack.layer.cornerRadius = 15
        stack.distribution = .fill
        stack.translatesAutoresizingMaskIntoConstraints = false
        
        return stack
    }()
    
    fileprivate let pediaLabel: UILabel = {
        let label = UILabel()
        label.text = "Pediatra"
        label.backgroundColor = .primaryColor
        label.textColor = .white
        label.textAlignment = .center
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let pediaImage: UIImageView = {
        let image = UIImage(named: Globals.img_pediatrics)
        let imageView = UIImageView(image: image)
        return imageView
    }()
    
    private func setupUI(){
//        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.primaryColor]
//        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.primaryColor]
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        navigationController?.navigationBar.tintColor = .white
        
        view.backgroundColor = .white
        view.addSubview(scrollView)
        
        scrollView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        scrollView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        scrollView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        scrollView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 10).isActive = true
        scrollView.showsVerticalScrollIndicator = false
        
        scrollView.addSubview(stackView)
        
        stackView.topAnchor.constraint(equalTo: scrollView.contentLayoutGuide.topAnchor).isActive = true
        stackView.leadingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.leadingAnchor).isActive = true
        stackView.trailingAnchor.constraint(equalTo: scrollView.contentLayoutGuide.trailingAnchor).isActive = true
        stackView.bottomAnchor.constraint(equalTo: scrollView.contentLayoutGuide.bottomAnchor).isActive = true
        stackView.widthAnchor.constraint(equalTo: scrollView.frameLayoutGuide.widthAnchor).isActive = true
         
        generalStackView.addArrangedSubview(generalLabel)
        generalStackView.addArrangedSubview(generalImage)
        stackView.addArrangedSubview(generalStackView)
        
        gineStackView.addArrangedSubview(gineLabel)
        gineStackView.addArrangedSubview(gineImage)
        stackView.addArrangedSubview(gineStackView)
        
        pediaStackView.addArrangedSubview(pediaLabel)
        pediaStackView.addArrangedSubview(pediaImage)
        stackView.addArrangedSubview(pediaStackView)
        
        generalStackView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        gineStackView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        pediaStackView.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        generalLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        generalImage.heightAnchor.constraint(equalToConstant: 250).isActive = true
        gineLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        gineImage.heightAnchor.constraint(equalToConstant: 200).isActive = true
        pediaLabel.heightAnchor.constraint(equalToConstant: 50).isActive = true
//        pediaImage.heightAnchor.constraint(equalToConstant: 200).isActive = true
        
        generalStackView.isHidden = true
        gineStackView.isHidden = true
        pediaStackView.isHidden = true
        
        if service?.medico_general == "S" {
            generalStackView.isHidden = false
        }
        if createAppointment?.beneficiaryId == 0 {
            if service?.ginecologia == "S" && service?.aplica_ginecologia == "S" {
                gineStackView.isHidden = false
            }
            if service?.pediatria != "S" && service?.aplica_pediatria == "S" {
                pediaStackView.isHidden = true
            }
        }
        else {
            service?.beneficiarios?.forEach({ (beneficiary) in
                if beneficiary.BENEFICIARIO == createAppointment?.beneficiaryId {
                    
                    if beneficiary.APLICA_GINECOLOGIA == "S" {
                        gineStackView.isHidden = false
                    }
                    if beneficiary.APLICA_PEDIATRIA == "S" {
                        pediaStackView.isHidden = false
                    }
                    
                }
            })
        }
        
        generalStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectGeneral)))
        gineStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectGine)))
        pediaStackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(selectPedia)))

    }
    
    @objc func selectGeneral(){
        createAppointment?.coverage = 101
        createAppointment?.coverageName = "Médico general"
        goToStep3()
    }
    
    @objc func selectGine(){
        createAppointment?.coverage = 102
        createAppointment?.coverageName = "Ginecólogo"
        goToStep3()
    }
    
    @objc func selectPedia(){
        createAppointment?.coverage = 104
        createAppointment?.coverageName = "Pediatra"
        goToStep3()
    }
    
    private func goToStep3(){
        guard let service = service, let createAppointment = createAppointment else {
            return
        }
        let vc = CASelectDoctorLocationViewController(with: service, createAppointment: createAppointment)
        vc.title = "Buscar médico"
        navigationController?.pushViewController(vc, animated: true)
    }
    

}
