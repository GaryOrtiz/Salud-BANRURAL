//
//  CAConfirmAppointmentViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import SCLAlertView

class CAConfirmAppointmentViewController: UIViewController {
    
    var service: Service?
    var createAppointment: CreateAppointment?
    let spinner = JGProgressHUD(style: .dark)
    
    init(with service: Service, createAppointment: CreateAppointment) {
        super.init(nibName: nil, bundle: nil)
        self.service = service
        self.createAppointment = createAppointment
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    fileprivate let stackView: UIStackView = {
        let stack = UIStackView()
        stack.backgroundColor = .white
        stack.axis = .vertical
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.clipsToBounds = true
        stack.layer.cornerRadius = 15
        return stack
    }()
    
    fileprivate let holderTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Titular"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let holderLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .primaryColor
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let pacientTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Paciente"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let pacientLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .primaryColor
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let doctorTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Médico"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let doctorLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .primaryColor
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let specialtyTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Especialidad"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let specialtyLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .primaryColor
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let dateTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Fecha"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let dateLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .primaryColor
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let hourTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Horario"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let hourLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .primaryColor
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let validateNumberTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Valida tu número de teléfono"
        label.textAlignment = .center
        label.textColor = .white
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let validateNumberLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = .primaryColor
        label.backgroundColor = .white
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let validateNumberText: UITextField = {
        let text = UITextField()
        text.keyboardType = .phonePad
        text.translatesAutoresizingMaskIntoConstraints = false
        text.backgroundColor = .primaryColor
        text.textColor = .white
        text.tintColor = .white
        text.attributedPlaceholder = NSAttributedString(string: "Ingresa tu número de teléfono",attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return text
    }()
    
    fileprivate let confirmButton: UIButton = {
        let button = UIButton()
        button.setTitle("Confirmar cita", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .primaryColor
        button.addTarget(CAConfirmAppointmentViewController.self, action: #selector(confirmAppointment), for: .touchUpInside)
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    private func setupUI(){
        view.backgroundColor = .white
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        navigationController?.navigationBar.tintColor = .white
        
        stackView.addArrangedSubview(holderTitleLabel)
        stackView.addArrangedSubview(holderLabel)
        stackView.addArrangedSubview(pacientTitleLabel)
        stackView.addArrangedSubview(pacientLabel)
        stackView.addArrangedSubview(doctorTitleLabel)
        stackView.addArrangedSubview(doctorLabel)
        stackView.addArrangedSubview(specialtyTitleLabel)
        stackView.addArrangedSubview(specialtyLabel)
        stackView.addArrangedSubview(dateTitleLabel)
        stackView.addArrangedSubview(dateLabel)
        stackView.addArrangedSubview(hourTitleLabel)
        stackView.addArrangedSubview(hourLabel)
        stackView.addArrangedSubview(validateNumberTitleLabel)
        stackView.addArrangedSubview(validateNumberLabel)
        stackView.addArrangedSubview(validateNumberText)
        
        validateNumberText.textAlignment = .center
        
        view.addSubview(stackView)
        view.addSubview(confirmButton)
        
        stackView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        
        holderLabel.text = service?.titular
        pacientLabel.text = createAppointment?.beneficiaryName
        doctorLabel.text = createAppointment?.doctorName
        specialtyLabel.text = createAppointment?.coverageName
        dateLabel.text = createAppointment?.selectedDate
        
        if let selectedDate = createAppointment?.selectedDate {
            let formmater = DateFormatter()
            formmater.dateFormat = "yyyy-MM-dd"
            if let date = formmater.date(from: selectedDate ) {
                formmater.dateFormat = "dd-MM-yyyy"
                dateLabel.text = formmater.string(from: date)
            }
        }
        if createAppointment?.selectedHour == "M" {
            hourLabel.text = "Mañana"
        }
        else if createAppointment?.selectedHour == "T" {
            hourLabel.text = "Tarde"
        }
        
        validateNumberText.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        confirmButton.topAnchor.constraint(equalTo: stackView.bottomAnchor, constant: 20).isActive = true
        confirmButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        confirmButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        confirmButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        let decoded = UserDefaults.standard.data(forKey: "user")
        let decoder = JSONDecoder()
        guard let data = decoded, let user = try? decoder.decode(User.self, from: data) else {
            presentErrorAlert(message: "No se pudo obtener el usuario.")
            return
        }
        validateNumberLabel.text = "******\(String(user.telefono).suffix(2))"
        confirmButton.backgroundColor = .acceptButtonColor
    }
    
    @objc private func confirmAppointment(){
        guard let phoneNumber = validateNumberText.text, !phoneNumber.isEmpty else {
            presentErrorAlert(message: "Valide su número de teléfono por favor.")
            return
        }
        guard let createAppointment = createAppointment, let transaction = createAppointment.transaction, let coverage = createAppointment.coverage else {
            return
        }
        validateNumberText.resignFirstResponder()
        self.spinner.show(in: self.view)
        let endpoint = "\(EndPoints.confirmarCita)"
        /*
         telefono: phoneNumber, transaccion: createAppointment.transaction, paciente: createAppointment.beneficiaryType, beneficiario: createAppointment.beneficiaryId, cobertura: createAppointment.coverage, sintomas: createAppointment.symptoms
         */
        let confirmAppointmentRequest = ConfirmAppointmentRequest(telefono: phoneNumber, transaccion: transaction, paciente: createAppointment.beneficiaryType, beneficiario: createAppointment.beneficiaryId, cobertura: coverage, sintomas: createAppointment.symptoms)
        
        print(confirmAppointmentRequest)
        
        SN.post(endpoint: endpoint, model: confirmAppointmentRequest ) { ( response: SNResultWithEntity<ConfirmAppointmentResponse, ErrorResponse>) in
            print(response)
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    /*guard let nc = self.navigationController else {
                        print("no navigation controller")
                        return
                    }
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let showTimeOut = SCLButton.ShowTimeoutConfiguration()
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Aceptar", backgroundColor: .primaryColor, textColor: .white, showTimeout: showTimeOut) {
                        nc.popToRootViewController(animated: true)
                    }
                    alertView.showSuccess("Cita creada", subTitle: "Tu código de cita es: \(data.codigo_cita)")*/
                    
                    let alert = AlertView()
                    alert.okButton.addTarget(self, action: #selector(self.finishAppointment), for: .touchUpInside)
                    alert.show(title: "Cita creada", subTitle: "Tu código de cita es: \(data.codigo_cita)", type: .success, iconColor: .secondaryColor, titleColor: .primaryColor, buttonColor: .secondaryColor)
                    self.view.addSubview(alert)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
        }
    }
    
    @objc func finishAppointment() {
        guard let nc = self.navigationController else {
            print("no navigation controller")
            return
        }
        nc.popToRootViewController(animated: true)
    }

}
