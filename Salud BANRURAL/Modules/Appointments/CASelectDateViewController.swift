//
//  CASelectDateViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import FSCalendar

class CASelectDateViewController: UIViewController {
    
    var service: Service?
    var createAppointment: CreateAppointment?
    var formatter = DateFormatter()
    private var schedule = [DoctorSchedule]()
    var dates = [String]()
    let spinner = JGProgressHUD(style: .dark)
    var hours = [DoctorScheduleHour]()
    var selectedDate: String = ""
    var selectedHour: String = ""
    
    init(with service: Service, createAppointment: CreateAppointment) {
        super.init(nibName: nil, bundle: nil)
        self.service = service
        self.createAppointment = createAppointment
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    fileprivate let calendar: FSCalendar = {
        let calendar = FSCalendar()
        calendar.locale = Locale(identifier: "es")
        calendar.calendarHeaderView.backgroundColor = .primaryColor
        calendar.appearance.headerTitleColor = .white
        calendar.appearance.weekdayTextColor = .primaryColor
        calendar.appearance.selectionColor = .primaryColor
        calendar.appearance.caseOptions = [.headerUsesUpperCase, .weekdayUsesSingleUpperCase]
        calendar.translatesAutoresizingMaskIntoConstraints = false
        return calendar
    }()
    
    fileprivate let selectedDateLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = label.font.withSize(18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let selectedHourLabel: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.font = label.font.withSize(18)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let hourPicker: UIPickerView = {
        let picker = UIPickerView()
        return picker
    }()
    
    fileprivate let hourText: UITextField = {
        let text = UITextField()
        text.text = "Selecciona un horario"
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.primaryColor.cgColor
        text.layer.cornerRadius = 5
        text.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 10, height: text.frame.height))
        text.leftViewMode = .always
        text.translatesAutoresizingMaskIntoConstraints = false
        return text
    }()
    
    fileprivate let reserveButton: UIButton = {
        let button = UIButton()
        button.setTitle("Reservar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .acceptButtonColor
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        getSchedule()
    }
    
    func getSchedule(){
        guard let doctorCode = createAppointment?.doctorCode else {
            return
        }
        
        self.spinner.show(in: self.view)
        let endpoint = "\(EndPoints.horariosMedico)/\(doctorCode)"
        print(endpoint)
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<DoctorScheduleResponse, ErrorResponse>) in
            print(response)
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    self.schedule = data.horarios
                    self.setupCalendar()
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }
    
    private func setupUI(){
        view.backgroundColor = .white
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        navigationController?.navigationBar.tintColor = .white
    }
    
    private func setupCalendar(){
        
        schedule.forEach { (day) in
            dates.append(day.fecha)
        }
        
        calendar.delegate = self
        calendar.dataSource = self
        view.addSubview(calendar)
        view.addSubview(selectedDateLabel)
        view.addSubview(selectedHourLabel)
        view.addSubview(reserveButton)
        hourText.inputView = hourPicker
        view.addSubview(hourText)
        hourPicker.delegate = self
        hourPicker.dataSource = self
        
        calendar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        calendar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 10).isActive = true
        calendar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -10).isActive = true
        calendar.heightAnchor.constraint(equalToConstant: 300).isActive = true
        
        hours.append(DoctorScheduleHour(codigo: "", descripcion: "Selecciona un horario"))
        hourPicker.reloadAllComponents()
        
        hourText.topAnchor.constraint(equalTo: calendar.bottomAnchor, constant: 20).isActive = true
        hourText.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        hourText.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        hourText.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        selectedDateLabel.topAnchor.constraint(equalTo: hourText.bottomAnchor, constant: 20).isActive = true
        selectedDateLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        selectedDateLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        selectedDateLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        selectedHourLabel.topAnchor.constraint(equalTo: selectedDateLabel.bottomAnchor, constant: 10).isActive = true
        selectedHourLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        selectedHourLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        selectedHourLabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        
        reserveButton.topAnchor.constraint(equalTo: selectedHourLabel.bottomAnchor, constant: 25).isActive = true
        reserveButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        reserveButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        reserveButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        reserveButton.addTarget(self, action: #selector(reserveAppointment), for: .touchUpInside)
        
        
    }
    
    @objc func reserveAppointment(){
        guard let service = service, var createAppointment = createAppointment,
              let doctorCode = createAppointment.doctorCode else {
            return
        }
        if selectedDate.isEmpty {
            presentErrorAlert(message: "Por favor selecciona una fecha")
            return
        }
        if selectedHour.isEmpty {
            presentErrorAlert(message: "Por favor selecciona un horario")
            return
        }
        createAppointment.selectedDate = selectedDate
        createAppointment.selectedHour = selectedHour
        
        self.spinner.show(in: self.view)
        let endpoint = "\(EndPoints.reservarCita)/\(doctorCode)"
        
        let reserveAppointmentRequest = ReserveAppointmentRequest(fecha: selectedDate, horario: selectedHour)
        
        SN.post(endpoint: endpoint, model: reserveAppointmentRequest ) { ( response: SNResultWithEntity<ReservationResponse, ErrorResponse>) in
            print(response)
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    if !data.reservado {
                        self.presentErrorAlert(message: "No se pudo reservar la cita.")
                        return
                    }
                    print(result)
                    createAppointment.transaction = result.transaccion
                    let vc = CAConfirmAppointmentViewController(with: service, createAppointment: createAppointment)
                    vc.title = "Confirmar cita"
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }

}

extension CASelectDateViewController: FSCalendarDelegate {
    
    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        formatter.dateFormat = "dd/MM/yyyy"
        selectedDateLabel.text = "Fecha: \(formatter.string(from: date))"
        
        formatter.dateFormat = "yyyy-MM-dd"
        selectedDate = formatter.string(from: date)
        schedule.forEach { (day) in
            if day.fecha == formatter.string(from: date){
                hours = day.horarios
                print(hours)
                hours.insert(DoctorScheduleHour(codigo: "", descripcion: "Selecciona un horario"), at: 0)
                hourPicker.reloadAllComponents()
            }
        }
        
    }
    
}

extension CASelectDateViewController: FSCalendarDataSource {
    
    func minimumDate(for calendar: FSCalendar) -> Date {
        formatter.dateFormat = "yyyy-MM-dd"
        
        guard let startDate = formatter.date(from: dates[0]) else {
            return Date()
        }
        return startDate
    }
    
    func maximumDate(for calendar: FSCalendar) -> Date {
        formatter.dateFormat = "yyyy-MM-dd"
        
        guard let endDate = formatter.date(from: dates[dates.count-1]) else {
            return Date().addingTimeInterval((24*60*60)*30)
        }
        return endDate
    }
    
    func calendar(_ calendar: FSCalendar, shouldSelect date: Date, at monthPosition: FSCalendarMonthPosition) -> Bool {
        formatter.dateFormat = "yyyy-MM-dd"
        return dates.contains(formatter.string(from: date))
    }
    
}

extension CASelectDateViewController: FSCalendarDelegateAppearance {
    
    func calendar(_ calendar: FSCalendar, appearance: FSCalendarAppearance, titleDefaultColorFor date: Date) -> UIColor? {
        formatter.dateFormat = "yyyy-MM-dd"
        if dates.contains(formatter.string(from: date)) {
            return .primaryColor
        }
        return .red
    }
    
}

extension CASelectDateViewController: UIPickerViewDelegate {
     
    // Sets the number of rows in the picker view
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return hours.count
    }
 
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        hourText.text = hours[row].descripcion
        selectedHour = hours[row].codigo
        
        schedule.forEach { (day) in
            if day.fecha == selectedDate {
                if hours[row].codigo == "M" {
                    selectedHourLabel.text = day.horario_m
                }
                else if hours[row].codigo == "M" {
                    selectedHourLabel.text = day.horario_t
                }
                else{
                    selectedHourLabel.text = ""
                }
            }
        }
    }
    
}

extension  CASelectDateViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return hours[row].descripcion
    }
    
}
