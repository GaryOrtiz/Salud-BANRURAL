//
//  CASelectDoctorByLocationViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/6/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import CoreLocation
import JGProgressHUD

class CASelectDoctorByLocationViewController: UIViewController {

    var service: Service?
    var createAppointment: CreateAppointment?
    private let cellId = "ProviderTableViewCell"
    private var dataSource = [Provider]()
    let spinner = JGProgressHUD(style: .dark)
    var distance: Float = 10
    private var locationManager: CLLocationManager?
    private var userLocation: CLLocation?
    
    init(with service: Service, createAppointment: CreateAppointment) {
        super.init(nibName: nil, bundle: nil)
        self.service = service
        self.createAppointment = createAppointment
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        requestLocation()
    }

    fileprivate let slider: UISlider = {
        let slider = UISlider()
        slider.tintColor = .primaryColor
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(sliderValueChanged), for: .valueChanged)
        return slider
    }()
    
    fileprivate let kmLabel: UILabel = {
       let label = UILabel()
        label.text = "Buscar médicos en 10.0 km a la redonda"
        label.textColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let searchButton: UIButton = {
       let button = UIButton()
        button.setTitle("Consultar", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .primaryColor
        button.translatesAutoresizingMaskIntoConstraints = false
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.addTarget(self, action: #selector(getProviders), for: .touchUpInside)
        return button
    }()
    
    fileprivate let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    private func setupUI(){
        
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        navigationController?.navigationBar.tintColor = .white
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        view.backgroundColor = .white
        
        slider.minimumValue = 5
        slider.maximumValue = 50
        slider.value = 10
        
        view.addSubview(kmLabel)
        view.addSubview(slider)
        view.addSubview(searchButton)
        view.addSubview(tableView)
        
        kmLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        kmLabel.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        kmLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        
        slider.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor, multiplier: 0.9).isActive = true
        slider.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        slider.topAnchor.constraint(equalTo: kmLabel.bottomAnchor, constant: 20).isActive = true
        
        searchButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        searchButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        searchButton.topAnchor.constraint(equalTo: slider.bottomAnchor, constant: 20).isActive = true
        searchButton.heightAnchor.constraint(equalToConstant: 50).isActive = true

        tableView.topAnchor.constraint(equalTo: searchButton.bottomAnchor, constant: 20).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0).isActive = true
 
    }
    
    @objc func sliderValueChanged(){
        let steps = Float(5)
        slider.value = roundf(slider.value/slider.maximumValue*steps)*slider.maximumValue/steps;
        kmLabel.text = "Buscar médicos en \(slider.value) km a la redonda"
        distance = slider.value
    }
    
    private func requestLocation(){
        //validar si tiene gps activo y disponible
        guard CLLocationManager.locationServicesEnabled() else {
            return
        }
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.desiredAccuracy = kCLLocationAccuracyBest
        locationManager?.requestAlwaysAuthorization()
        locationManager?.startUpdatingLocation()
        
    }
    
    @objc func getProviders(){
        guard let userLocation = userLocation else {
            presentErrorAlert(message: "No se pudo obtener tu ubicación.")
            return
        }
        guard let coverage = createAppointment?.coverage else {
            return
        }
        
        self.spinner.show(in: self.view)
        let lat = userLocation.coordinate.latitude
        let lon = userLocation.coordinate.longitude
        let endpoint = "\(EndPoints.medicosPorUbicacion)/\(coverage)/\(lat)/\(lon)/\(distance)"
        print(endpoint)
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<ProvidersResponse, ErrorResponse>) in
            print(response)
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    self.dataSource = data.proveedores
                    self.tableView.reloadData()
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
        
    }

}

//MARK: - UITableViewDataSource
extension CASelectDoctorByLocationViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath)

        if let cell = cell as? ProviderTableViewCell{
            cell.setupCellWith(provider: dataSource[indexPath.row], position: indexPath.row)
        }
        
        return cell
    }
    
}

extension CASelectDoctorByLocationViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        createAppointment?.doctorCode = dataSource[indexPath.row].codigo
        createAppointment?.doctorName = dataSource[indexPath.row].descripcion
        createAppointment?.doctorAddress = dataSource[indexPath.row].direccion
        
        guard let service = service, let createAppointment = createAppointment else {
            return
        }
        
        let vc = CASelectDateViewController(with: service, createAppointment: createAppointment)
        vc.title = "Horario"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? ProviderTableViewCell else {
            return
        }
        
        if(indexPath.row % 2 == 0){
            cell.backgroundColor = .providerBackground
            cell.nameLabel.textColor = .providerName
            cell.distanceLabel.textColor = .providerDistance
            cell.addressLabel.textColor = .providerAddress
            cell.countryLabel.textColor = .providerState
        }
        else{
            cell.backgroundColor = .providerBackgroundTint
            cell.nameLabel.textColor = .providerNameTint
            cell.distanceLabel.textColor = .providerDistanceTint
            cell.addressLabel.textColor = .providerAddressTint
            cell.countryLabel.textColor = .providerStateTint
        }
    }
}

extension CASelectDoctorByLocationViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let bestLocation = locations.last else {
            return
        }
        userLocation = bestLocation
    }
}
