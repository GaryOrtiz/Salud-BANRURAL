//
//  ProfileViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/8/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
//import OneSignal
import KeychainSwift

class ProfileViewController: UIViewController {
    
    let spinner = JGProgressHUD(style: .dark)
    var service: Service?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    fileprivate let holderTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Titular"
        label.textColor = .profileTitle
        label.font = UIFont.boldSystemFont(ofSize: 28.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let holderLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .profileSubtitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let separator01Label: UILabel = {
        let label = UILabel()
        label.text = ""
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let planTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Plan"
        label.textColor = .profileTitle
        label.font = UIFont.boldSystemFont(ofSize: 28.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let planLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .profileSubtitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let separator02Label: UILabel = {
        let label = UILabel()
        label.text = ""
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let certificateTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Certificado"
        label.textColor = .profileTitle
        label.font = UIFont.boldSystemFont(ofSize: 28.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let certificateLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .profileSubtitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let separator03Label: UILabel = {
        let label = UILabel()
        label.text = ""
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let startDateTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Fecha de inicio"
        label.textColor = .profileTitle
        label.font = UIFont.boldSystemFont(ofSize: 28.0)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let startDateLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = .profileSubtitle
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let separator04Label: UILabel = {
        let label = UILabel()
        label.text = ""
        label.backgroundColor = .primaryColor
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    fileprivate let logoutButton: UIButton = {
        let button = UIButton()
        button.setTitle("Cerrar sesión", for: .normal)
        button.setTitleColor(.white, for: .normal)
        button.backgroundColor = .secondaryColor
        button.addTarget(ProfileViewController.self, action: #selector(logout), for: .touchUpInside)
        button.clipsToBounds = true
        button.layer.cornerRadius = 15
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    fileprivate let stack: UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.spacing = 10
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    func setupUI(){
        
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
        
        view.addSubview(stack)
        
        stack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 20).isActive = true
        stack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        stack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        
        stack.addArrangedSubview(holderTitleLabel)
        stack.addArrangedSubview(holderLabel)
        stack.addArrangedSubview(separator01Label)
        stack.addArrangedSubview(planTitleLabel)
        stack.addArrangedSubview(planLabel)
        stack.addArrangedSubview(separator02Label)
        stack.addArrangedSubview(certificateTitleLabel)
        stack.addArrangedSubview(certificateLabel)
        stack.addArrangedSubview(separator03Label)
        stack.addArrangedSubview(startDateTitleLabel)
        stack.addArrangedSubview(startDateLabel)
        stack.addArrangedSubview(separator04Label)
        
        stack.addArrangedSubview(logoutButton)
        
        logoutButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 50).isActive = true
        logoutButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true
        logoutButton.heightAnchor.constraint(equalToConstant: 50).isActive = true
        logoutButton.topAnchor.constraint(equalTo: separator04Label.bottomAnchor, constant: 20).isActive = true
        
        separator01Label.heightAnchor.constraint(equalToConstant: 2).isActive = true
        separator02Label.heightAnchor.constraint(equalToConstant: 2).isActive = true
        separator03Label.heightAnchor.constraint(equalToConstant: 2).isActive = true
        separator04Label.heightAnchor.constraint(equalToConstant: 2).isActive = true
        
        
        stack.isHidden = true
        getService()
    }
    
    private func getService(){
        self.spinner.show(in: self.view)
        
        let decoded = UserDefaults.standard.data(forKey: "user")
        let decoder = JSONDecoder()
        guard let data = decoded, let user = try? decoder.decode(User.self, from: data) else {
            presentErrorAlert(message: "No se pudo obtener el usuario.")
            return
        }
//        OneSignal.removeExternalUserId()
        let endpoint = "\(EndPoints.contrato)"
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity<ServiceResponse, ErrorResponse>) in
            
            self.spinner.dismiss()
            
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    let service = data.contratos
                    
                    self.holderLabel.text = service.titular
                    self.planLabel.text = service.plan
                    self.startDateLabel.text = service.fecha_inicio
                    self.certificateLabel.text = service.contrato_fisico
                    self.stack.isHidden = false
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }
    
    @objc func logout(){
        //remove user
        UserDefaults.standard.removeObject(forKey: "user")
        //remove token
        let keychain = KeychainSwift()
        keychain.delete("token")
        //remove onesignal
//        OneSignal.removeExternalUserId()
        
        let endpoint = "\(EndPoints.logout)"
        self.spinner.show(in: self.view)
        
        SN.get(endpoint: endpoint ) { ( response: SNResultWithEntity< LogoutResponse, ErrorResponse>) in
            
            self.spinner.dismiss()
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let loginVC = storyboard.instantiateViewController(withIdentifier: "loginViewController")
//                loginVC.modalPresentationStyle = .popover
//                self.navigationController?.pushViewController(loginVC, animated: true)
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            appDelegate.window?.rootViewController = loginVC
//            self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
            
        }
    }

}
