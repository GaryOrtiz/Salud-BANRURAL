//
//  ChatTableViewCell.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class ChatTableViewCell: UITableViewCell {

    //MARK: - Outlets
    
    @IBOutlet weak var userImage: UIImageView!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setupCellWith(chat: Chat) {
        userNameLabel.text = chat.operador
        messageLabel.text = chat.texto_inicio
        dateLabel.text = chat.fecha
        
        userNameLabel.textColor = .primaryColor
    }
    
}
