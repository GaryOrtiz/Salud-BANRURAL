//
//  MessageTextTableViewCell.swift
//  EPSS
//
//  Created by Informatica EPSS on 1/6/21.
//  Copyright © 2021 EPSS. All rights reserved.
//

import UIKit

class MessageTextTableViewCell: UITableViewCell {

    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageBackgroundView: UIView!
    var trailingConstraint: NSLayoutConstraint!
    var leadingConstraint: NSLayoutConstraint!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        messageLabel.text = nil
        leadingConstraint.isActive = false
        trailingConstraint.isActive = false
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    public func setupCellWith(message: ChatMessage, senderId: Int) {
        messageBackgroundView.layer.cornerRadius = 10
        messageBackgroundView.clipsToBounds = true
        messageLabel.text = message.mensaje
        
        trailingConstraint = messageBackgroundView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20)
        leadingConstraint = messageBackgroundView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 20)
        
        if senderId == message.envia_user_id {
            messageLabel.textColor = .textReceivedMessage
            messageBackgroundView.backgroundColor = .bubbleReceivedMessage
            trailingConstraint.isActive = true
            messageLabel.textAlignment = .left
        }
        else {
            messageLabel.textColor = .textSentMessage
            messageBackgroundView.backgroundColor = .bubbleSentMessage
            leadingConstraint.isActive = true
            messageLabel.textAlignment = .left
        }
    }
    
}
