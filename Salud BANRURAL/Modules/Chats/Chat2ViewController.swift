//
//  Chat2ViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 1/5/21.
//  Copyright © 2021 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import SDWebImage
import ImageViewer
import PusherSwift
//import OneSignal
import PDFKit

class CustomView: UIView {

    override var intrinsicContentSize: CGSize {
        return CGSize.zero
    }
}

class Chat2ViewController: UITableViewController {
    
    private var observation: NSKeyValueObservation?
    private var pdfURL: URL?
    
    private var senderId: Int {
        var senderId = 0
        
        if let savedUser = UserDefaults.standard.object(forKey: "user") as? Data {
            let decoder = JSONDecoder()
            if let user = try? decoder.decode(User.self, from: savedUser){
                senderId = user.id
            }
        }
        return senderId
    }
    
    var customInputView: UIView!
    var sendButton: UIButton!
    var addMediaButtom: UIButton!
    var inputBarStackView: UIStackView!
    let textField = FlexibleTextView()
    override var inputAccessoryView: UIView? {
        if customInputView == nil {
            customInputView = CustomView()
            customInputView.backgroundColor = .lightGray
            textField.placeholder = "Escribe un mensaje ..."
            textField.font = .systemFont(ofSize: 17)
            textField.layer.cornerRadius = 5

            customInputView.autoresizingMask = .flexibleHeight

            customInputView.addSubview(textField)

            sendButton = UIButton()
            sendButton.isEnabled = true
            sendButton.setImage(UIImage(named: "paperplane.fill"), for: .normal)
            sendButton.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            sendButton.backgroundColor = .primaryColor
            sendButton.tintColor = .white
            sendButton.layer.cornerRadius = 17
            sendButton.addTarget(self, action: #selector(sendMessage), for: .touchUpInside)
            customInputView.addSubview(sendButton)

            addMediaButtom = UIButton()
            sendButton.isEnabled = true
            addMediaButtom.setImage(UIImage(named: "paperclip"), for: .normal)
            addMediaButtom.tintColor = .systemBlue
            addMediaButtom.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
            addMediaButtom.clipsToBounds = true
            addMediaButtom.layer.cornerRadius = 17
            addMediaButtom.addTarget(self, action: #selector(showActionSheet), for: .touchUpInside)
            customInputView.addSubview(addMediaButtom)

            textField.translatesAutoresizingMaskIntoConstraints = false
            sendButton.translatesAutoresizingMaskIntoConstraints = false
            addMediaButtom.translatesAutoresizingMaskIntoConstraints = false

            sendButton.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)
            sendButton.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)

            addMediaButtom.setContentHuggingPriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)
            addMediaButtom.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000), for: NSLayoutConstraint.Axis.horizontal)

            textField.maxHeight = 80

            NSLayoutConstraint.activate([
                addMediaButtom.widthAnchor.constraint(equalToConstant: 34),
                addMediaButtom.heightAnchor.constraint(equalToConstant: 34),
                addMediaButtom.leadingAnchor.constraint(equalTo: customInputView.leadingAnchor, constant: 8),
                addMediaButtom.trailingAnchor.constraint(equalTo: textField.leadingAnchor, constant: -8),
                addMediaButtom.bottomAnchor.constraint(equalTo: customInputView.layoutMarginsGuide.bottomAnchor, constant: -8),
            ])


            NSLayoutConstraint.activate([
                textField.trailingAnchor.constraint(equalTo: sendButton.leadingAnchor, constant: -8),
                textField.topAnchor.constraint(equalTo: customInputView.topAnchor, constant: 8),
                textField.bottomAnchor.constraint(equalTo: customInputView.layoutMarginsGuide.bottomAnchor, constant: -8),
            ])

            NSLayoutConstraint.activate([
                sendButton.widthAnchor.constraint(equalToConstant: 34),
                sendButton.heightAnchor.constraint(equalToConstant: 34),
                sendButton.leadingAnchor.constraint(equalTo: textField.trailingAnchor, constant: 0),
                sendButton.trailingAnchor.constraint(equalTo: customInputView.trailingAnchor, constant: -8),
                sendButton.bottomAnchor.constraint(equalTo: customInputView.layoutMarginsGuide.bottomAnchor, constant: -8),
            ])

        }
        return customInputView

    }
    
    override var canBecomeFirstResponder: Bool {
        return true
    }
    
    private var chatId: Int = 0
    private var operatorName: String = ""
    private var messages = [ChatMessage]()
    private var images: [ImageItem] = []
    private var pusher: Pusher!
    let videoCallButton = UIButton(type: .custom)
    let spinner = JGProgressHUD(style: .dark)
    let messageTextCellId = "MessageTextTableViewCell"
    let messageImageCellId = "MessageImageTableViewCell"
    let messageDocumentCellId = "MessageDocumentTableViewCell"
    var bottomConstraint: NSLayoutConstraint!

    init(with chatId: Int, operatorName: String) {
        self.chatId = chatId
        self.operatorName = operatorName
        UserDefaults.standard.setValue(chatId, forKey: "chatId")
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        setupPusher()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        observation?.invalidate()
        print("WILL DISAPPEAR")
        UserDefaults.standard.removeObject(forKey: "chatId")
        /*NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceivedNotification"), object: nil)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;*/
        
        if self.isMovingFromParent {
            tabBarController?.tabBar.isHidden = false
        }
        guard let pusher = pusher else {
            print("no pusher")
            return
        }
        pusher.disconnect()
        print("pusher disconnect")
        NotificationCenter.default.removeObserver(self)
    }
    
    @objc func receivedNotification(_ notification: Notification) {
        let notificationPayload =  notification.userInfo
        guard let chatIdFromNotification = notificationPayload?["chat_id"] as? Int else {
            return
        }
        /*if chatId == chatIdFromNotification {
            OneSignal.inFocusDisplayType = OSNotificationDisplayType.none //Notification is silent and not shown
        }
        else{
            OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        }*/
        //check the message belongs to this room then if you want show your local notification , if you want do nothing
    }
    
    private func setupPusher()
    {
        let options = PusherClientOptions(
            host: .cluster("us2"),
            useTLS: true
        )
        self.pusher = Pusher(key: "9fee5a93cb1755fc376d", options: options)
        pusher.delegate = self
        print(pusher.connection.connectionState.stringValue())
        let channel = pusher.subscribe("PublicChatEvent")
        let event = "public_chat.\(self.chatId)"
        channel.bind(eventName: event, callback: { (data: Any?) -> Void in
            guard let data = data as? [String : AnyObject],
                    let evento = data["evento"] as? String else {
                return
            }
            let mensaje = data["mensaje"] as AnyObject
            if(evento == "enviar_mensaje"){
                
                let id = mensaje["id"] as! Int
                let sala_chat_id = mensaje["sala_chat_id"] as! Int
                let message = mensaje["mensaje"] as! String
                let envia_user_id = mensaje["envia_user_id"] as! Int
                let envia = mensaje["envia"] as! String
                let avatar = mensaje["avatar"] as! String
                let hora = mensaje["hora"] as! String
                let ruta = mensaje["ruta"] as? String
                let nombre_archivo = mensaje["nombre_archivo"] as? String
                let tipo = mensaje["tipo"] as! String
                
                let chatMessage = ChatMessage(id: id, sala_chat_id: sala_chat_id, fecha: nil, mensaje: message, envia_user_id: envia_user_id, envia: envia, avatar: avatar, hora: hora, ruta: ruta, nombre_archivo: nombre_archivo, tipo: tipo)
                
                self.addMessage(message: chatMessage)
                
            }
            if(evento == "cerrar_chat"){
                
                let id = mensaje["id"] as! Int
                let sala_chat_id = mensaje["sala_chat_id"] as! Int
                let message = mensaje["mensaje"] as! String
                let envia_user_id = (mensaje["envia_user_id"] != nil) ? 0:mensaje["envia_user_id"] as! Int
                let envia = mensaje["envia"] as! String
                let avatar = mensaje["avatar"] as! String
                let hora = mensaje["hora"] as! String
                let ruta = mensaje["ruta"] as? String
                let nombre_archivo = mensaje["nombre_archivo"] as? String
                let tipo = mensaje["tipo"] as! String
                let chatMessage = ChatMessage(id: id, sala_chat_id: sala_chat_id, fecha: nil, mensaje: message, envia_user_id: envia_user_id, envia: envia, avatar: avatar, hora: hora, ruta: ruta, nombre_archivo: nombre_archivo, tipo: tipo)
                
                self.addMessage(message: chatMessage)
            }
            if(evento == "trasladar_chat"){
            
                let id = mensaje["id"] as! Int
                let sala_chat_id = mensaje["sala_chat_id"] as! Int
                let message = mensaje["mensaje"] as! String
                let envia_user_id = (mensaje["envia_user_id"] != nil) ? 0:mensaje["envia_user_id"] as! Int
                let envia = mensaje["envia"] as! String
                let avatar = mensaje["avatar"] as! String
                let hora = mensaje["hora"] as! String
                let ruta = mensaje["ruta"] as? String
                let nombre_archivo = mensaje["nombre_archivo"] as? String
                let tipo = mensaje["tipo"] as! String
                let chatMessage = ChatMessage(id: id, sala_chat_id: sala_chat_id, fecha: nil, mensaje: message, envia_user_id: envia_user_id, envia: envia, avatar: avatar, hora: hora, ruta: ruta, nombre_archivo: nombre_archivo, tipo: tipo)
                
                self.addMessage(message: chatMessage)
            }
        })
        pusher.connect()
    }
    
    private func setupUI(){
        tabBarController?.tabBar.isHidden = true
        
        navigationItem.title = operatorName
        navigationController?.navigationBar.tintColor = .white
        
        let background = UIImage(named: Globals.chat_pattern)
        let imageView = UIImageView(frame: view.bounds)
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.image = background
        tableView.backgroundColor = .white

        let backgroundImage = UIImageView(image: background)
        backgroundImage.contentMode = .scaleAspectFit
        let tap = UITapGestureRecognizer(target: self, action: #selector(tableTapped))
        
        let tableBGView = UIView()
        tableBGView.addSubview(backgroundImage)
        
        tableView.backgroundView = tableBGView
        tableView.backgroundView?.addGestureRecognizer(tap)
        
        tableView.register(UINib(nibName: messageTextCellId, bundle: nil), forCellReuseIdentifier: messageTextCellId)
        tableView.register(UINib(nibName: messageImageCellId, bundle: nil), forCellReuseIdentifier: messageImageCellId)
        tableView.register(UINib(nibName: messageDocumentCellId, bundle: nil), forCellReuseIdentifier: messageDocumentCellId)
        tableView.keyboardDismissMode = .interactive
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
            
        getMessages()
        
//        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIWindow.keyboardWillShowNotification, object: nil)
//        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIWindow.keyboardWillHideNotification, object: nil)
    }

    
    
    @objc func tableTapped() {
        hideKeyboard()
    }
    
    func hideKeyboard() {
        textField.resignFirstResponder()
        textField.endEditing(true)
    }
    
    
    @objc func sendMessage() {
        
        guard let text = textField.text, !text.replacingOccurrences(of: " ", with: "").isEmpty else {
            return
        }
        
        self.spinner.show(in: self.view)
        
        let newMessage = NewMessageRequest(sala_chat_id: self.chatId, mensaje: text, tipo: "TXT", base64: nil)
        
        SN.post(endpoint: "\(EndPoints.enviarMensaje)", model: newMessage) { ( response: SNResultWithEntity<ChatMessageResponse, ErrorResponse>) in
            self.spinner.dismiss()
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    self.textField.text = ""
                    guard let datos = result.datos else {
                        return
                    }
                    self.addMessage(message: datos.mensaje)
                }
                else{
                    self.hideKeyboard()
                    self.presentErrorAlert(message: result.mensaje)
                }
            case .error(let error):
                self.hideKeyboard()
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.hideKeyboard()
                self.presentErrorAlert(message: entity.mensaje)
            }
        }
    }
    
    @objc func showActionSheet() {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let photoAction = UIAlertAction(title: "Fotos", style: .default, handler: { [weak self] _ in
            let picker = UIImagePickerController()
            picker.sourceType = .photoLibrary
            picker.delegate = self
//                picker.allowsEditing = true
            self?.present(picker, animated: true)
        })
        let photoImage = UIImage(named: "photo")
        photoAction.setValue(photoImage, forKey: "image")
        
        let cameraAction = UIAlertAction(title: "Cámara", style: .default, handler: { [weak self] _ in
            let picker = UIImagePickerController()
            picker.sourceType = .camera
            picker.delegate = self
//                picker.allowsEditing = true
            self?.present(picker, animated: true)
        })
        let cameraImage = UIImage(named: "camera")
        cameraAction.setValue(cameraImage, forKey: "image")
        
        actionSheet.addAction(photoAction)
        actionSheet.addAction(cameraAction)
        actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
        self.present(actionSheet,animated: true)
    }
    
    private func setupButtons(){
        videoCallButton.isHidden = true
        videoCallButton.setImage(UIImage(named: "video"), for: .normal)
        videoCallButton.addTarget(self, action: #selector(videocall), for: .touchUpInside)
        videoCallButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                videoCallButton.tintColor = .white
            }
        }
        let videoCallBarButton = UIBarButtonItem(customView: videoCallButton)
        videoCallBarButton.customView?.widthAnchor.constraint(equalToConstant: 25).isActive = true
        videoCallBarButton.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.rightBarButtonItems = [videoCallBarButton]
    }
    
    @objc func videocall()
    {
        let vc = VideoCallViewController(with: self.chatId)
        vc.navigationItem.largeTitleDisplayMode = .never
        vc.modalPresentationStyle = .fullScreen
        vc.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    private func getMessages(){
        self.spinner.show(in: self.view)
        
        SN.get(endpoint: "\(EndPoints.chat)/\(self.chatId)") { ( response: SNResultWithEntity<ChatResponse, ErrorResponse>) in
            self.spinner.dismiss()
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    let chat = data.chat
                    if(chat.permite_videollamada == 1){
                        self.videoCallButton.isHidden = false
                    }
                    chat.mensajes?.forEach({ (message) in
                        
                        self.addMessage(message: message)
                        
                    })
                    self.tableView.reloadData()
                    self.tableView.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: .top, animated: false)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }
    
    private func addMessage(message: ChatMessage){
        if message.tipo == "IMG" {
            appendImageMessage(message: message)
        }
        else {
            messages.append(message)
            self.tableView.reloadData()
            self.tableView.scrollToRow(at: IndexPath(row: self.messages.count - 1, section: 0), at: .top, animated: false)
        }
    }
    
    private func appendImageMessage(message: ChatMessage) {
        guard let imageURL = URL(string: message.ruta ?? "") else {
            return
        }
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.sd_setImage(with: imageURL, completed: nil)
        imageView.sd_setImage(with: imageURL) { (image, error, cacheType, url) in
            let galleryItem = GalleryItem.image { $0(image) }
            self.images.append(ImageItem(id: message.id, url: imageURL, imageView: imageView, galleryItem: galleryItem))
        }
        messages.append(message)
    }

}

extension Chat2ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage,
            let imageData = image.jpegData(compressionQuality: 10) else {
            return
        }
        let newMessage = NewMessageRequest(sala_chat_id: self.chatId, mensaje: "", tipo: "IMG", base64: imageData.base64EncodedString())
        print(newMessage)
        self.spinner.show(in: self.view)
        SN.post(endpoint: "\(EndPoints.enviarMensaje)", model: newMessage) { ( response: SNResultWithEntity<ChatMessageResponse, ErrorResponse>) in
            self.spinner.dismiss()
            print(response)
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let datos = result.datos else {
                        return
                    }
                    self.addMessage(message: datos.mensaje)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
            case .error(let error):
                print(error)
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
        }
    }
    
}

extension Chat2ViewController: GalleryItemsDataSource {
    
    func itemCount() -> Int {
        print("Images: \(images.count)")
        return images.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return images[index].galleryItem
    }
    
}

extension Chat2ViewController: PusherDelegate {
    
    func changedConnectionState(from old: ConnectionState, to new: ConnectionState) {
        print("old: \(old.stringValue()) -> new: \(new.stringValue())")
    }
    
    func subscribedToChannel(name: String) {
        print("Subscribed to channel \(name)")
    }
    
    func debugLog(message: String) {
        print("DebugLog: \(message)")
    }
    
    func receivedError(error: PusherError) {
        if let code = error.code {
            print("Received Error: (\(code)) \(error.message)")
        }
        else{
            print("Recieved Error: \(error.message)")
        }
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return messages.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let message = messages[indexPath.row]
        if message.tipo == "IMG" {
            let cell = tableView.dequeueReusableCell(withIdentifier: messageImageCellId, for: indexPath)
            cell.selectionStyle = .none
            if let cell = cell as? MessageImageTableViewCell{
                let current = messages[indexPath.row]
                cell.setupCellWith(message: current, senderId: self.senderId)
            }
            return cell
        }
        else if message.tipo == "DOC" {
            let cell = tableView.dequeueReusableCell(withIdentifier: messageDocumentCellId, for: indexPath)
            cell.selectionStyle = .none
            if let cell = cell as? MessageDocumentTableViewCell{
                let current = messages[indexPath.row]
                cell.setupCellWith(message: current, senderId: self.senderId)
            }
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: messageTextCellId, for: indexPath)
            cell.selectionStyle = .none
            if let cell = cell as? MessageTextTableViewCell{
                let current = messages[indexPath.row]
                cell.setupCellWith(message: current, senderId: self.senderId)
            }
            return cell
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: false)
        self.hideKeyboard()
    
        let message = messages[indexPath.row]

        if message.tipo == "IMG" {
            guard let ruta = message.ruta, let imageUrl = URL(string: ruta) else {
                return
            }
            guard let imageViewIndex = self.images.firstIndex(where: { $0.url == imageUrl }) else {
                return
            }
            let galleryViewController = GalleryViewController(startIndex: imageViewIndex, itemsDataSource: self, configuration: [.thumbnailsButtonMode(.none),.deleteButtonMode(.none)])
            self.presentImageGallery(galleryViewController)
        }
        else if message.tipo == "DOC" {
//            let progressView = UIView()
//            progressView.backgroundColor = .red
//
            guard let navView = self.navigationController?.view else { return }
//
//            navView.addSubview(progressView)
//            progressView.translatesAutoresizingMaskIntoConstraints = false
//            progressView.heightAnchor.constraint(equalToConstant: 50).isActive = true
//            progressView.leadingAnchor.constraint(equalTo: navView.leadingAnchor, constant: 75).isActive = true
//            progressView.trailingAnchor.constraint(equalTo: navView.trailingAnchor, constant: -75).isActive = true
//            progressView.centerYAnchor.constraint(equalTo: navView.centerYAnchor).isActive = true
            spinner.show(in: navView)
            
            guard let ruta = message.ruta else {
                return
            }
            guard let urlToDownload = URL(string: ruta) else { return }
                    
            let urlSession = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
            
            let downloadTask = urlSession.downloadTask(with: urlToDownload)
            downloadTask.resume()
        }
        
    }
    
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = .transparent
    }
    
}

extension Chat2ViewController:  URLSessionDownloadDelegate {
    func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
            print("downloadLocation:", location)
            // create destination URL with the original pdf name
            guard let url = downloadTask.originalRequest?.url else { return }
            let documentsPath = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask)[0]
            let destinationURL = documentsPath.appendingPathComponent(url.lastPathComponent)
            // delete original copy
            try? FileManager.default.removeItem(at: destinationURL)
            // copy from temp to Document
            do {
                try FileManager.default.copyItem(at: location, to: destinationURL)
                self.pdfURL = destinationURL
                DispatchQueue.main.async {
                    self.spinner.dismiss()
                    let vc = PDFViewerViewController(with: destinationURL)
                    self.navigationController?.pushViewController(vc, animated: true)
                }
            } catch let error {
                print("Copy Error: \(error.localizedDescription)")
            }
        }
}

////MARK: - UITableViewDataSource
//extension Chat2ViewController: UITableViewDataSource{
//
//
//}
//
//extension Chat2ViewController: UITableViewDelegate {
//
//
//}

/*extension Chat2ViewController: InputBarAccessoryViewDelegate {
    
    // MARK: - InputBarAccessoryViewDelegate
    
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {

        inputBar.inputTextView.text = String()
        inputBar.invalidatePlugins()

        // Send button activity animation
        inputBar.sendButton.startAnimating()
        inputBar.inputTextView.placeholder = "Sending..."
        //Enviar
        sleep(2)
        inputBar.sendButton.stopAnimating()
        inputBar.inputTextView.placeholder = "Aa"
        
    }
    
}*/
