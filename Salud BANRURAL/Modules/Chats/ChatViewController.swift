//
//  ChatViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/7/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import MessageKit
import InputBarAccessoryView
import Simple_Networking
import JGProgressHUD
import SDWebImage
import ImageViewer
import PusherSwift
//import OneSignal

struct Message: MessageType {
    var sender: SenderType
    var messageId: String
    var sentDate: Date
    var kind: MessageKind
}

struct Sender: SenderType {
    var senderId: String
    var displayName: String
}

struct Media: MediaItem {
    var url: URL?
    var image: UIImage?
    var placeholderImage: UIImage
    var size: CGSize
}

struct ImageItem {
    let id: Int
    let url: URL
    let imageView: UIImageView
    let galleryItem: GalleryItem
}

class ChatViewController: MessagesViewController{

    //MARK: - Properties
    private var chatId: Int = 0
    private var messages = [Message]()
    private var images: [ImageItem] = []
    private var pusher: Pusher!
    let videoCallButton = UIButton(type: .custom)
    
    private var selfSender: Sender? {
        if let savedUser = UserDefaults.standard.object(forKey: "user") as? Data {
            let decoder = JSONDecoder()
            if let user = try? decoder.decode(User.self, from: savedUser){
                return Sender(senderId: "\(user.id)", displayName: user.username)
            }
            return nil
        }
        return nil
    }
        
    let spinner = JGProgressHUD(style: .dark)
    
    init(with chatId: Int) {
        self.chatId = chatId
        UserDefaults.standard.setValue(chatId, forKey: "chatId")
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupPusher()
    {
        let options = PusherClientOptions(
            host: .cluster("us2"),
            useTLS: true
        )
        self.pusher = Pusher(key: "ba57a42037f9ec0dc761", options: options)
        pusher.delegate = self
        print(pusher.connection.connectionState.stringValue())
        let channel = pusher.subscribe("PublicChatEvent")
        let event = "public_chat.\(self.chatId)"
        channel.bind(eventName: event, callback: { (data: Any?) -> Void in
            guard let data = data as? [String : AnyObject],
                    let evento = data["evento"] as? String else {
                return
            }
            guard let mensaje = data["mensaje"] else {
                return
            }
            if(evento == "enviar_mensaje"){
                
                let id = mensaje["id"] as! Int
                let sala_chat_id = mensaje["sala_chat_id"] as! Int
                let message = mensaje["mensaje"] as! String
                let envia_user_id = mensaje["envia_user_id"] as! Int
                let envia = mensaje["envia"] as! String
                let avatar = mensaje["avatar"] as! String
                let hora = mensaje["hora"] as! String
                let ruta = mensaje["ruta"] as? String
                let nombre_archivo = mensaje["nombre_archivo"] as? String
                let tipo = mensaje["tipo"] as! String
                
                let chatMessage = ChatMessage(id: id, sala_chat_id: sala_chat_id, fecha: nil, mensaje: message, envia_user_id: envia_user_id, envia: envia, avatar: avatar, hora: hora, ruta: ruta, nombre_archivo: nombre_archivo, tipo: tipo)
                
                self.addMessage(message: chatMessage)
                
            }
        })
        pusher.connect()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        /*NotificationCenter.default.addObserver(self,
               selector: #selector(receivedNotification(_:)),
               name: NSNotification.Name(rawValue: "ReceivedNotification"), object: nil)*/
        
        messageInputBar.inputTextView.placeholder = "Escribe un mensaje..."
        messageInputBar.sendButton.title = "Enviar"
        setupPusher()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        print("WILL DISAPPEAR")
        UserDefaults.standard.removeObject(forKey: "chatId")
        /*NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: "ReceivedNotification"), object: nil)
        OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;*/
        
        if self.isMovingFromParent {
            tabBarController?.tabBar.isHidden = false
        }
        guard let pusher = pusher else {
            print("no pusher")
            return
        }
        pusher.disconnect()
        print("pusher disconnect")
    }
    
    @objc func receivedNotification(_ notification: Notification) {
        let notificationPayload =  notification.userInfo
        guard let chatIdFromNotification = notificationPayload?["chat_id"] as? Int else {
            return
        }
        /*if chatId == chatIdFromNotification {
            OneSignal.inFocusDisplayType = OSNotificationDisplayType.none //Notification is silent and not shown
        }
        else{
            OneSignal.inFocusDisplayType = OSNotificationDisplayType.notification;
        }*/
        //check the message belongs to this room then if you want show your local notification , if you want do nothing
    }
    
    private func setupUI(){
        tabBarController?.tabBar.isHidden = true
        
        let background = UIImage(named: "pattern")
        let imageView = UIImageView(frame: view.bounds)
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.image = background
        imageView.center = view.center
        self.messagesCollectionView.backgroundView = imageView
        
        messagesCollectionView.messagesDataSource = self
        messagesCollectionView.messagesLayoutDelegate = self
        messagesCollectionView.messagesDisplayDelegate = self
        messagesCollectionView.messageCellDelegate = self
        messageInputBar.delegate = self
        
        messagesCollectionView.backgroundView?.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapOnMessages)))
        
        guard let layout = messagesCollectionView.collectionViewLayout as? MessagesCollectionViewFlowLayout else {
            return
        }
        layout.setMessageOutgoingAvatarSize(.zero)
        layout.setMessageIncomingAvatarSize(.zero)
        
        setupButtons()
        getMessages()
    }
    
    private func setupButtons(){
        //VideocallButton
        videoCallButton.isHidden = true
        videoCallButton.setImage(UIImage(named: "video"), for: .normal)
        videoCallButton.addTarget(self, action: #selector(videocall), for: .touchUpInside)
        videoCallButton.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                videoCallButton.tintColor = .white
            }
        }
        let videoCallBarButton = UIBarButtonItem(customView: videoCallButton)
        videoCallBarButton.customView?.widthAnchor.constraint(equalToConstant: 25).isActive = true
        videoCallBarButton.customView?.heightAnchor.constraint(equalToConstant: 25).isActive = true
        self.navigationItem.rightBarButtonItems = [videoCallBarButton]
        //Button for upload images
        let button = InputBarButtonItem()
        button.setSize(CGSize(width: 35, height: 35), animated: false)
        button.setImage(UIImage(named: "plus"), for: .normal)
        button.onTouchUpInside { (InputBarButtonItem) in
            let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let photoAction = UIAlertAction(title: "Fotos", style: .default, handler: { [weak self] _ in
                let picker = UIImagePickerController()
                picker.sourceType = .photoLibrary
                picker.delegate = self
//                picker.allowsEditing = true
                self?.present(picker, animated: true)
            })
            let photoImage = UIImage(named: "photo")
            photoAction.setValue(photoImage, forKey: "image")
            
            let cameraAction = UIAlertAction(title: "Cámara", style: .default, handler: { [weak self] _ in
                let picker = UIImagePickerController()
                picker.sourceType = .camera
                picker.delegate = self
//                picker.allowsEditing = true
                self?.present(picker, animated: true)
            })
            let cameraImage = UIImage(named: "camera")
            cameraAction.setValue(cameraImage, forKey: "image")
            
            actionSheet.addAction(photoAction)
            actionSheet.addAction(cameraAction)
            actionSheet.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: nil))
            self.present(actionSheet,animated: true)
        }
        
        messageInputBar.setLeftStackViewWidthConstant(to: 36, animated: false)
        messageInputBar.setStackViewItems([button], forStack: .left, animated: false)
        
        scrollsToLastItemOnKeyboardBeginsEditing = true
    }
    
    @objc func tapOnMessages(){
        messageInputBar.inputTextView.resignFirstResponder()
    }
    
    @objc func videocall()
    {
        let vc = VideoCallViewController(with: self.chatId)
        vc.navigationItem.largeTitleDisplayMode = .never
        vc.modalPresentationStyle = .fullScreen
        vc.navigationController?.isNavigationBarHidden = true
        self.navigationController?.pushViewController(vc, animated: false)
    }
    
    private func getMessages(){
        self.spinner.show(in: self.view)
        
        SN.get(endpoint: "\(EndPoints.chat)/\(self.chatId)") { ( response: SNResultWithEntity<ChatResponse, ErrorResponse>) in
            self.spinner.dismiss()
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let data = result.datos else {
                        return
                    }
                    let chat = data.chat
                    if(chat.permite_videollamada == 1){
                        self.videoCallButton.isHidden = false
                    }
                    chat.mensajes?.forEach({ (message) in
                        
                        self.addMessage(message: message)
                        
                    })
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
                
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }
    
    private func addMessage(message: ChatMessage){
        if message.tipo == "IMG" {
            appendImageMessage(message: message)
        }
        else {
            appendTextMessage(message: message)
        }
        self.messagesCollectionView.reloadData()
        self.messagesCollectionView.scrollToBottom()
    }
    
    private func appendTextMessage(message: ChatMessage){
        let font = UIFont.systemFont(ofSize: 18)
        let attributes: [NSAttributedString.Key: Any] = [
            .font: font
        ]
        let attributedMessage = NSAttributedString(string: message.mensaje, attributes: attributes)
        self.messages.append(Message(
            sender: Sender(senderId: "\(message.envia_user_id)", displayName: message.envia),
            messageId: "\(message.id)",
            sentDate: Date(),
            kind: .attributedText(attributedMessage)))
    
    }
    
    private func appendImageMessage(message: ChatMessage){
        guard let imageURL = URL(string: message.ruta ?? ""),
                let imagePlaceholder = UIImage(named: "diagnostico") else {
            return
        }
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.sd_setImage(with: imageURL, completed: nil)
        imageView.sd_setImage(with: imageURL) { (image, error, cacheType, url) in
            let galleryItem = GalleryItem.image { $0(image) }
            self.images.append(ImageItem(id: message.id, url: imageURL, imageView: imageView, galleryItem: galleryItem))
        }
        self.messages.append(Message(
            sender: Sender(senderId: "\(message.envia_user_id)", displayName: message.envia),
            messageId: "\(message.id)",
            sentDate: Date(),
            kind: .photo(Media(url: imageURL, image: nil, placeholderImage: imagePlaceholder, size: CGSize(width: 250, height: 250 )))))
        
        if(!message.mensaje.isEmpty){
            appendTextMessage(message: message)
        }
        
    }

}

extension ChatViewController: MessagesDataSource, MessagesLayoutDelegate, MessagesDisplayDelegate, MessageCellDelegate {
    
    func currentSender() -> SenderType {
        if let sender = self.selfSender {
            return sender
        }
        return Sender(senderId: "0",displayName: "")
    }
    
    func messageForItem(at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageType {
        return self.messages[indexPath.section]
    }
    
    func numberOfSections(in messagesCollectionView: MessagesCollectionView) -> Int {
        return self.messages.count
    }
    
    func didTapImage(in cell: MessageCollectionViewCell) {
        guard let indexPath = messagesCollectionView.indexPath(for: cell) else {
            return
        }
        let message = messages[indexPath.section]
        switch message.kind {
        case .photo(let media):
            guard let imageUrl = media.url else {
                return
            }
            guard let imageViewIndex = self.images.firstIndex(where: { $0.url == imageUrl }) else {
                return
            }
            let galleryViewController = GalleryViewController(startIndex: imageViewIndex, itemsDataSource: self, configuration: [.thumbnailsButtonMode(.none),.deleteButtonMode(.none)])
            self.presentImageGallery(galleryViewController)
            
        default:
            break
        }
    }
    
    func didTapMessage(in cell: MessageCollectionViewCell) {
        self.messageInputBar.inputTextView.resignFirstResponder()
    }
    
    func configureAvatarView(_ avatarView: AvatarView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
        avatarView.isHidden = true
    }
    
    func configureMediaMessageImageView(_ imageView: UIImageView, for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) {
       
        guard let message = message as? Message else {
            return
        }
        switch message.kind {
        case .photo(let media):
            guard let imageUrl = media.url else {
                return
            }
            imageView.sd_setImage(with: imageUrl, completed: nil)
        default:
            break
        }
    }
    
    func backgroundColor(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> UIColor {
        if self.selfSender?.senderId == message.sender.senderId {
            return UIColor(red: 210/255, green: 236/255, blue: 192/255, alpha: 1)
        }
        return .white
    }
    
    func messageStyle(for message: MessageType, at indexPath: IndexPath, in messagesCollectionView: MessagesCollectionView) -> MessageStyle {
        /*var corners: UIRectCorner = []
        corners.formUnion(.topLeft)
        corners.formUnion(.bottomLeft)
        corners.formUnion(.topRight)
        corners.formUnion(.bottomRight)
        return .custom { view in
            let radius: CGFloat = 5
            let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            view.layer.mask = mask
        }*/
        let tail: MessageStyle.TailCorner = isFromCurrentSender(message: message) ? .bottomRight : .bottomLeft
        return .bubbleTail(tail, .curved)
    }
    
}

extension ChatViewController: InputBarAccessoryViewDelegate {
    func inputBar(_ inputBar: InputBarAccessoryView, didPressSendButtonWith text: String) {
        guard !text.replacingOccurrences(of: " ", with: "").isEmpty else {
            return
        }
        
        self.spinner.show(in: self.view)
        
        let newMessage = NewMessageRequest(sala_chat_id: self.chatId, mensaje: text, tipo: "TXT", base64: nil)
        
        SN.post(endpoint: "\(EndPoints.enviarMensaje)", model: newMessage) { ( response: SNResultWithEntity<ChatMessageResponse, ErrorResponse>) in
            self.spinner.dismiss()
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    inputBar.inputTextView.text = ""
                    guard let datos = result.datos else {
                        return
                    }
                    self.addMessage(message: datos.mensaje)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
        }
    }
}

extension ChatViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true, completion: nil)
        guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage,
            let imageData = image.pngData() else {
            return
        }
        let newMessage = NewMessageRequest(sala_chat_id: self.chatId, mensaje: "", tipo: "IMG", base64: imageData.base64EncodedString())
        self.spinner.show(in: self.view)
        SN.post(endpoint: "\(EndPoints.enviarMensaje)", model: newMessage) { ( response: SNResultWithEntity<ChatMessageResponse, ErrorResponse>) in
            self.spinner.dismiss()
            print(response)
            switch response {
            
            case .success(let result):
                if(result.resultado){
                    guard let datos = result.datos else {
                        return
                    }
                    self.addMessage(message: datos.mensaje)
                }
                else{
                    self.presentErrorAlert(message: result.mensaje)
                }
            case .error(let error):
                self.presentErrorAlert(message: error.localizedDescription)
            case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
        }
    }
    
}

extension ChatViewController: GalleryItemsDataSource {
    
    func itemCount() -> Int {
        print("Images: \(images.count)")
        return images.count
    }
    
    func provideGalleryItem(_ index: Int) -> GalleryItem {
        return images[index].galleryItem
    }
    
}

extension ChatViewController: PusherDelegate {
    
    func changedConnectionState(from old: ConnectionState, to new: ConnectionState) {
        print("old: \(old.stringValue()) -> new: \(new.stringValue())")
    }
    
    func subscribedToChannel(name: String) {
        print("Subscribed to channel \(name)")
    }
    
    func debugLog(message: String) {
        print("DebugLog: \(message)")
    }
    
    func receivedError(error: PusherError) {
        if let code = error.code {
            print("Received Error: (\(code)) \(error.message)")
        }
        else{
            print("Recieved Error: \(error.message)")
        }
    }
    
}
