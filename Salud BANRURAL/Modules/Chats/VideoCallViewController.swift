//
//  VideoCallViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/23/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import Simple_Networking
import JGProgressHUD
import OpenTok
import SCLAlertView

class VideoCallViewController: UIViewController {

    //MARK: - Properties
    private var chatId: Int = 0
    private var API_KEY: String = ""
    private var SESSION_ID: String = ""
    private var TOKEN: String = ""
    private var VIDEOCALL_ID: Int = 0
    private var session: OTSession?
    private var publisher: OTPublisher?
    private var subscriber: OTSubscriber?
    
    let buttonsStack = UIStackView()
    let muteButton = UIButton(type: .custom)
    let cancelButton = UIButton(type: .custom)
    let noVideoButton = UIButton(type: .custom)
    let toggleCameraButton = UIButton(type: .custom)
    private var isMute = false;
    private var isFrontCamera = true;
    private var isVideoOn = true;
    
    let spinner = JGProgressHUD(style: .dark)
    
    let backgroundView: UIView = {
        let view = UIView()
        view.frame = UIScreen.main.bounds
        view.backgroundColor = .primaryColor
        
        let image = UIImage(named: "logo_white")
        let imageView = UIImageView(image: image)
        imageView.frame = view.bounds
        imageView.contentMode = .scaleAspectFit
        
        view.addSubview(imageView)
        
        return view
        
    }()
    
    init(with chatId: Int) {
        self.chatId = chatId
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        tabBarController?.tabBar.isHidden = true
        getVideoCall()
    }
    
    func setupUI(){
        navigationController?.isNavigationBarHidden = true
        modalPresentationStyle = .fullScreen
        UIApplication.shared.isIdleTimerDisabled = true
//        setupButtons()
    }
    
    func setupButtons()
    {
        let screenBounds = UIScreen.main.bounds
        let sizeButton: CGFloat = CGFloat((Double(screenBounds.width)/6))
        let padding: CGFloat = 15.0
        
        let toggleCameraView = UIView()
//        let noVideoView = UIView()
        let muteView = UIView()
        let cancelView = UIView()
        
        toggleCameraView.addSubview(toggleCameraButton)
//        noVideoView.addSubview(noVideoButton)
        muteView.addSubview(muteButton)
        cancelView.addSubview(cancelButton)
        
        buttonsStack.addArrangedSubview(toggleCameraView)
//        buttonsStack.addArrangedSubview(noVideoView)
        buttonsStack.addArrangedSubview(muteView)
        buttonsStack.addArrangedSubview(cancelView)
        
        toggleCameraButton .translatesAutoresizingMaskIntoConstraints = false
        toggleCameraButton.widthAnchor.constraint(equalToConstant: sizeButton).isActive = true
        toggleCameraButton.heightAnchor.constraint(equalToConstant: sizeButton).isActive = true
        toggleCameraButton.layer.cornerRadius = 0.5 * sizeButton
        toggleCameraButton.centerXAnchor.constraint(equalTo: toggleCameraView.centerXAnchor).isActive = true
        toggleCameraButton.centerYAnchor.constraint(equalTo: toggleCameraView.centerYAnchor).isActive = true
        toggleCameraButton.clipsToBounds = true
        toggleCameraButton.backgroundColor = .primaryColor
        toggleCameraButton.setImage(UIImage(named: "camera.rotate.fill"), for: .normal)
        toggleCameraButton.contentHorizontalAlignment = .fill
        toggleCameraButton.contentVerticalAlignment = .fill
        toggleCameraButton.imageView?.contentMode = .scaleAspectFill
        toggleCameraButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        toggleCameraButton.tintColor = .white
        toggleCameraButton.addTarget(self, action: #selector(toggleCamera), for: .touchUpInside)
        
        /*noVideoButton .translatesAutoresizingMaskIntoConstraints = false
        noVideoButton.widthAnchor.constraint(equalToConstant: sizeButton).isActive = true
        noVideoButton.heightAnchor.constraint(equalToConstant: sizeButton).isActive = true
        noVideoButton.layer.cornerRadius = 0.5 * sizeButton
        noVideoButton.centerXAnchor.constraint(equalTo: noVideoView.centerXAnchor).isActive = true
        noVideoButton.centerYAnchor.constraint(equalTo: noVideoView.centerYAnchor).isActive = true
        noVideoButton.clipsToBounds = true
        noVideoButton.backgroundColor = .red
        noVideoButton.setImage(UIImage(named: "video"), for: .normal)
        if #available(iOS 13.0, *) {
            
        } else {
            // Fallback on earlier versions
        }
        noVideoButton.contentHorizontalAlignment = .fill
        noVideoButton.contentVerticalAlignment = .fill
        noVideoButton.imageView?.contentMode = .scaleAspectFit
        noVideoButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        noVideoButton.tintColor = .white
        noVideoButton.addTarget(self, action: #selector(toggleVideo), for: .touchUpInside)*/
        
        muteButton .translatesAutoresizingMaskIntoConstraints = false
        muteButton.widthAnchor.constraint(equalToConstant: sizeButton).isActive = true
        muteButton.heightAnchor.constraint(equalToConstant: sizeButton).isActive = true
        muteButton.layer.cornerRadius = 0.5 * sizeButton
        muteButton.centerXAnchor.constraint(equalTo: muteView.centerXAnchor).isActive = true
        muteButton.centerYAnchor.constraint(equalTo: muteView.centerYAnchor).isActive = true
        muteButton.clipsToBounds = true
        muteButton.backgroundColor = .primaryColor
        muteButton.setImage(UIImage(named: "mic.slash.fill"), for: .normal)
        muteButton.contentHorizontalAlignment = .fill
        muteButton.contentVerticalAlignment = .fill
        muteButton.imageView?.contentMode = .scaleAspectFill
        muteButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        muteButton.tintColor = .white
        muteButton.addTarget(self, action: #selector(toggleMute), for: .touchUpInside)
        
        cancelButton .translatesAutoresizingMaskIntoConstraints = false
        cancelButton.widthAnchor.constraint(equalToConstant: sizeButton).isActive = true
        cancelButton.heightAnchor.constraint(equalToConstant: sizeButton).isActive = true
        cancelButton.layer.cornerRadius = 0.5 * sizeButton
        cancelButton.centerXAnchor.constraint(equalTo: cancelView.centerXAnchor).isActive = true
        cancelButton.centerYAnchor.constraint(equalTo: cancelView.centerYAnchor).isActive = true
        cancelButton.clipsToBounds = true
        cancelButton.backgroundColor = .red
        cancelButton.setImage(UIImage(named: "xmark"), for: .normal)
        cancelButton.contentHorizontalAlignment = .fill
        cancelButton.contentVerticalAlignment = .fill
        cancelButton.imageView?.contentMode = .scaleAspectFill
        cancelButton.contentEdgeInsets = UIEdgeInsets(top: padding, left: padding, bottom: padding, right: padding)
        cancelButton.tintColor = .white
        cancelButton.addTarget(self, action: #selector(closeVideoCall), for: .touchUpInside)
        
        buttonsStack.axis = .horizontal
        buttonsStack.alignment = .fill
        buttonsStack.distribution = .fillEqually
        buttonsStack.frame = CGRect(x: 0, y: screenBounds.height - 150, width: screenBounds.width, height: 150)
        buttonsStack.backgroundColor = .whiteTransparentColor
        buttonsStack.layer.cornerRadius = 15
        buttonsStack.layer.zPosition = .greatestFiniteMagnitude
        
        view.addSubview(buttonsStack)
        
        
    }
    
    @objc func closeVideoCall(){
        _ = navigationController?.popViewController(animated: true)
    }
    
    @objc func toggleMute(){
        print("muting")
        guard let publisher = self.publisher else {
            print("error muting")
            return
        }
        isMute = !isMute
        publisher.publishAudio = !isMute
        if(isMute){
            muteButton.setImage(UIImage(named: "mic.fill"), for: UIControl.State.normal)
            muteButton.backgroundColor = .white
            muteButton.tintColor = .primaryColor
            
        }
        else{
            muteButton.setImage(UIImage(named: "mic.slash.fill"), for: UIControl.State.normal)
            muteButton.backgroundColor = .primaryColor
            muteButton.tintColor = .white
        }
    }
    
    @objc func toggleCamera(){
        guard let publisher = self.publisher else {
            return
        }
        isFrontCamera = !isFrontCamera
        if(isFrontCamera){
            publisher.cameraPosition = .front
        }
        else{
            publisher.cameraPosition = .back
        }
    }
    
    @objc func toggleVideo(){
        guard let publisher = self.publisher else {
            return
        }
        isVideoOn = !isVideoOn
        publisher.publishVideo = isVideoOn
        if(isVideoOn){
            if #available(iOS 13.0, *) {
                noVideoButton.setImage(UIImage(systemName: "video.slash.fill"), for: UIControl.State.normal)
            } else {
                // Fallback on earlier versions
            }
        }
        else{
            if #available(iOS 13.0, *) {
                noVideoButton.setImage(UIImage(systemName: "video.fill"), for: UIControl.State.normal)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    
    func getVideoCall(){
        self.spinner.show(in: self.view)
        
        let request = VideoCallRequest(sala_chat_id: self.chatId)
        
        SN.post(endpoint: "\(EndPoints.videollamada)", model: request) { ( response: SNResultWithEntity<VideoCallResponse, ErrorResponse>) in
            self.spinner.dismiss()
            switch response {
                case .success(let result):
                    if(result.resultado){
                        guard let datos = result.datos else {
                            return
                        }
                        self.API_KEY = datos.apiKey
                        self.SESSION_ID = datos.sessionId
                        self.TOKEN = datos.token
                        self.VIDEOCALL_ID = datos.videollamadaId
                        self.connectToAnOpenTokSession()
                    }
                    else{
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let showTimeOut = SCLButton.ShowTimeoutConfiguration()
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Aceptar", backgroundColor: .primaryColor, textColor: .white, showTimeout: showTimeOut) {
                            self.navigationController?.popViewController(animated: true)
                        }
                        alertView.showError("¡Oops!", subTitle: result.mensaje)
                        
                    }
                    
                case .error(let error):
                    self.presentErrorAlert(message: error.localizedDescription)
                case .errorResult(let entity):
                self.presentErrorAlert(message: entity.mensaje)
            }
            
        }
    }
    
    func connectToAnOpenTokSession(){
        session = OTSession(apiKey: API_KEY, sessionId: SESSION_ID, delegate: self, settings: nil)
        var error: OTError?
        session?.connect(withToken: TOKEN, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        if self.isMovingFromParent {
            navigationController?.isNavigationBarHidden = false
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        let touch:UITouch? = touches.first
        if touch?.view != buttonsStack {
            if buttonsStack.isHidden {
                viewSlideInFromBottomToTop(view: buttonsStack)
                buttonsStack.isHidden = false
            }
            else{
                viewSlideInFromTopToBottom(view: buttonsStack)
                buttonsStack.isHidden = true
            }
            
        }
    }
    
    func viewSlideInFromTopToBottom(view: UIView){
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromBottom
        view.layer.add(transition, forKey: kCATransition)
    }
    
    func viewSlideInFromBottomToTop(view: UIView){
        let transition: CATransition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        transition.type = CATransitionType.push
        transition.subtype = CATransitionSubtype.fromTop
        view.layer.add(transition, forKey: kCATransition)
    }

}

extension VideoCallViewController: OTSessionDelegate {
    
    func sessionDidConnect(_ session: OTSession) {
        print("The client connected to the OpenToek session")
        
        let settings = OTPublisherSettings()
        settings.name = UIDevice.current.name
        guard let publisher = OTPublisher(delegate: self, settings: settings) else {
            return
        }
        self.publisher = publisher
        var error: OTError?
        session.publish(publisher, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let publisherView = publisher.view else {
            return
        }
        
        let screenBounds = UIScreen.main.bounds
        publisherView.frame = CGRect(x: screenBounds.width - 170, y: 50, width: 150, height: 200)
        publisherView.clipsToBounds = true
        publisherView.layer.cornerRadius = 15
//        publisherView.layer.maskedCorners = [.layerMaxXMaxYCorner]
        view.addSubview(publisherView)
        setupButtons()
    }
    
    func sessionDidDisconnect(_ session: OTSession) {
        print("the client disconnected from the OpenTok session")
    }
    
    func session(_ session: OTSession, didFailWithError error: OTError) {
        print("The client failed to connect to the OpenTok Session \(error)")
    }
    
    func session(_ session: OTSession, streamCreated stream: OTStream) {
        print("A stream was created in the session")
        
        subscriber = OTSubscriber(stream: stream, delegate: self)
        guard let subscriber = subscriber else {
            return
        }
        
        var error: OTError?
        session.subscribe(subscriber, error: &error)
        guard error == nil else {
            print(error!)
            return
        }
        
        guard let subscriberView = subscriber.view else {
            return
        }
        subscriberView.frame = UIScreen.main.bounds
        subscriberView.addSubview(backgroundView)
        view.insertSubview(subscriberView, at: 0)
//        setupButtons()
    }
    
    func session(_ session: OTSession, streamDestroyed stream: OTStream) {
        print("A stream was destroyed in the session")
        
        let alert = UIAlertController(title: "Videollamada finalizada!", message: "La videollamada ha sido finalizada.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Aceptar", style: .cancel, handler: { [weak self] _ in
            _ = self?.navigationController?.popViewController(animated: true)
        }))
        self.present(alert, animated: true)
    }
    
}

extension VideoCallViewController: OTPublisherDelegate {
    
    func publisher(_ publisher: OTPublisherKit, didFailWithError error: OTError) {
        print("The publisher faild \(error)")
    }
    
}

extension VideoCallViewController: OTSubscriberDelegate {
    
    func subscriberDidConnect(toStream subscriber: OTSubscriberKit) {
        print("The subscriber did connect to the stream")
    }
    
    func subscriber(_ subscriber: OTSubscriberKit, didFailWithError error: OTError) {
        print("The subscriber faild to connect to the stream")
    }
    
    func subscriberVideoDisabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        guard let subscriberView = self.subscriber?.view else {
            return
        }
        subscriberView.addSubview(backgroundView)
    }
    
    func subscriberVideoEnabled(_ subscriber: OTSubscriberKit, reason: OTSubscriberVideoEventReason) {
        backgroundView.removeFromSuperview()
    }
    
}
