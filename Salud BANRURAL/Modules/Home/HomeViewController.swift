//
//  HomeViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 9/28/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit
import SCLAlertView
import RevealingSplashView
import Simple_Networking
import KeychainSwift

class HomeViewController: UIViewController {
    
    let cellId = "ImageCardTableViewCell"
    var dataSource = [ImageCard]()
    
    fileprivate let tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        
//        let splashView = RevealingSplashView(iconImage: UIImage(named: "logo_white")!, iconInitialSize: CGSize(width: 300, height: 90), backgroundColor: .primaryColor)
//
//        view.addSubview(splashView)
//        splashView.animationType = .popAndZoomOut
//        splashView.
//        splashView.startAnimation()
////        splashView.duration = 5
//        splashView.delay = 5
//
//        Timer.scheduledTimer(withTimeInterval: 5, repeats: false) { (timer) in
//            splashView.heartAttack = true
//        }
        
//        UserDefaults.standard.removeObject(forKey: "user")
        if let _ = UserDefaults.standard.object(forKey: "user") as? Data {
            let keychain = KeychainSwift()
            guard let token = keychain.get("token") else
            {
                return
            }
            SimpleNetworking.setAuthenticationHeader(prefix: "bearer", token: token)
            setupUI()
        }
        else {
            let storyboard = UIStoryboard(name: "Login", bundle: nil)
            let loginVC = storyboard.instantiateViewController(withIdentifier: "loginViewController")
//                loginVC.modalPresentationStyle = .popover
//                navigationController?.pushViewController(loginVC, animated: true)
                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                appDelegate.window?.rootViewController = loginVC
            
        }
    }
    
    private func setupUI(){
        self.navigationController?.navigationBar.largeTitleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        navigationItem.largeTitleDisplayMode = .never
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = .primaryColor
        self.navigationController?.navigationBar.barTintColor = .primaryColor
        self.navigationController?.navigationBar.isHidden = false
        let logo = UIImage(named: Globals.logo_white)
        let imageView = UIImageView(image: logo)
        imageView.contentMode = .scaleAspectFit
        self.navigationItem.titleView = imageView
//        tabBarController?.tabBar.isHidden = true
        
        let medicalAssistanceCard = ImageCard(title: "¿Necesitas una asesoría médica?", image: Globals.img_medical_assistance, type: "medical_assistance")
        let makeAppointmentCard = ImageCard(title: "¿Deseas agendar una cita?", image: Globals.img_make_appointment, type: "make_appointment")
        let chatWithUsCard = ImageCard(title: "Chatea con nosotros", image: Globals.img_chat_with_us, type: "chat_with_us")
        let medicalNetworkCard = ImageCard(title: "Consulta nuestra red médica", image: Globals.img_medical_network, type: "medical_network")
        
        dataSource.append(medicalAssistanceCard)
        dataSource.append(makeAppointmentCard)
        dataSource.append(chatWithUsCard)
        dataSource.append(medicalNetworkCard)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
        tableView.separatorStyle = .none
        tableView.showsVerticalScrollIndicator = false
        
        
        view.addSubview(tableView)
        
        tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        
        self.tabBarController?.tabBar.barTintColor = .primaryColor
        self.tabBarController?.tabBar.isTranslucent = false
        self.tabBarController?.tabBar.tintColor = .secondaryColor
        self.tabBarController?.tabBar.unselectedItemTintColor = .white

    }
    
    @objc func finishAppointment() {
        let pop = NewChatPopUp()
        pop.typeOfPopup = "medicalAssistance"
        view.addSubview(pop)
    }
    
    func createMedicalAssistanceChat(){
        let pop = NewChatPopUp()
        pop.typeOfPopup = "medicalAssistance"
        view.addSubview(pop)
    }
    
    func makeAppointment(){
        let vc = CASelectBeneficiaryViewController()
        vc.title = "Crear cita"
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func createChat(){
        let pop = NewChatPopUp()
        pop.typeOfPopup = "chat"
        view.addSubview(pop)
    }
    
    func goToMedicalNetwork(){
        
        let storyboard = UIStoryboard(name: "MedicalNetwork", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MedicalNetworkViewControllerID") as UIViewController
        vc.title = "Red médica"
        navigationController?.pushViewController(vc, animated: true)
    }
    
}

//MARK: - UITableViewDataSource
extension HomeViewController: UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: self.cellId, for: indexPath)

        if let cell = cell as? ImageCardTableViewCell{
            cell.setupCellWith(imageCard: dataSource[indexPath.row])
        }
        
        return cell
    }
    
}

extension HomeViewController: UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let card = dataSource[indexPath.row]
        if card.type == "medical_assistance" {
            createMedicalAssistanceChat()
        }
        else if card.type == "make_appointment" {
            makeAppointment()
        }
        else if card.type == "chat_with_us" {
            createChat()
        }
        else if card.type == "medical_network" {
            goToMedicalNetwork()
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        guard let cell = cell as? ImageCardTableViewCell else {
            return
        }
        cell.selectionStyle = .none
        cell.backgroundColor = .white
        cell.stackView.clipsToBounds = true
        cell.stackView.layer.cornerRadius = 15
        cell.titleLabel.backgroundColor = .primaryColor
        cell.titleLabel.textColor = .white
        
    }
}
