//
//  HomeTabBarViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 10/8/20.
//  Copyright © 2020 EPSS. All rights reserved.
//

import UIKit

class HomeTabBarViewController: UITabBarController {

    @IBOutlet weak var tabBarView: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if #available(iOS 12.0, *) {
            if self.traitCollection.userInterfaceStyle == .dark {
                tabBarView.tintColor = .white
            }
        }
    }

}
