//
//  LaunchScreenViewController.swift
//  EPSS
//
//  Created by Informatica EPSS on 1/12/21.
//  Copyright © 2021 EPSS. All rights reserved.
//

import UIKit

class LaunchScreenViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupUI()
    }
    
    func setupUI() {
        view.backgroundColor = .primaryColor
    }

}
